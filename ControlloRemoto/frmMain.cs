﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using FluentFTP;
using System.Threading;
using System.IO;
using WaitWnd;

namespace ControlloRemoto
{
    public partial class frmMain : Form
    {
        private Connectionlist connectionlist = new Connectionlist();

        string m_host = "Forno_rotor";
        string m_user = "Rotor";
        string m_pass = "logiudice";
        int m_port = 20;

        static ManualResetEvent m_reset = new ManualResetEvent(false);
        WaitWndFun waitForm = new WaitWndFun();
        static FtpClient ftpClient;

        static FtpClient NewFtpClient(string host, int port, string username, string password)
        { 
               return new FtpClient(host, port, new NetworkCredential(username, password));
        }

        public frmMain()
        {
            InitializeComponent();

            Global.ParStr = new ClParStr();
            Global.ricette = new ClRicette();

            //List<string> listaFileRemoti = new List<string>();

            //using (FtpClient cl = NewFtpClient(m_host, m_port, m_user, m_pass))
            //{
            //    //cl.Port = m_port;
            //    cl.Connect();

            //    // 100 K file
            //    //cl.UploadFile(@"D:\Github\hgupta\FluentFTP\README.md", "/public_html/temp/README.md");
            //    //cl.DownloadFile(@"D:\Github\hgupta\FluentFTP\README2.md", "/public_html/temp/README.md");
            //    cl.DownloadFile(@"C:\DatiFornoRotor\Prog.dat","/Programmi/rotor_13/Prog.dat");
            //    //cl.BeginSetWorkingDirectory("/Programmi/rotor_13/Immagini/", new AsyncCallback(BeginSetWorkingDirectoryCallback), Global.conn);
            //    cl.SetWorkingDirectory("/Programmi/rotor_13/Immagini/");

            //    string[] tempor = cl.GetNameListing("/Programmi/rotor_13/Immagini/");
            //    FtpListItem[] ftpli = cl.GetListing("/Programmi/rotor_13/Immagini/", FtpListOption.ForceList | FtpListOption.Recursive);
            //    foreach (FtpListItem i in ftpli)
            //    {                   
            //        //FtpTrace.WriteLine(i);

            //        listaFileRemoti.Add("/Programmi/rotor_13/Immagini/" + SplitString(i.ToString())[1]);
            //    }
            //    //FtpListParser ftplp = new FtpListParser(cl);
            //    //ftplp.Init("WINDOWS");
            //    //ftpli = ftplp.ParseMultiLine(listaFileRemoti.ToArray(), true);
            //    //listaFileRemoti.Clear();
            //    //foreach (FtpListItem i in ftpli)
            //    //{
            //    //    //FtpTrace.WriteLine(i);
            //    //    listaFileRemoti.Add(i.ToString());
            //    //}
            //    // download many
            //    cl.DownloadFiles(@"C:\DatiFornoRotor\Immagini", listaFileRemoti.ToArray(), false);
            //    //new string[] { @"/public_html/temp/file0.exe", @"/public_html/temp/file1.exe", @"/public_html/temp/file2.exe", @"/public_html/temp/file3.exe", @"/public_html/temp/file4.exe" }, false);

            //    //cl.DownloadFiles(@"C:\DatiFornoRotor\Immagini", "/Programmi/rotor_13/Immagini");


            //    // 10 M file
            //    //cl.UploadFile(@"D:\Drivers\mb_driver_intel_irst_6series.exe", "/public_html/temp/big.txt");
            //    //cl.Rename("/public_html/temp/big.txt", "/public_html/temp/big2.txt");
            //    //cl.DownloadFile(@"C:\DatiFornoRotor\IA3W_config_visualizza.do.pdf", "/Programmi/rotor_13/IA3W_config_visualizza.do.pdf");

            //    cl.Dispose();
            //}

            ////BeginDownloadFiles(listaFileRemoti.ToArray(), new AsyncCallback(BeginSetWorkingDirectoryCallback), Global.conn);
            ////await DownloadImageAsync(listaFileRemoti.ToArray());
            ////Global.conn = new FtpClient();

            ////Global.conn.Host = "Forno_rotor";//"localhost";
            ////Global.conn.Credentials = new NetworkCredential("Rotor", "logiudice");//("ftptest", "ftptest");
            ////Global.conn.Port = 20;
            //////conn.DeleteDirectory("/test");
            ////System.Diagnostics.Debug.WriteLine("The working directory is: " + Global.conn.GetWorkingDirectory());

            ////Global.conn.BeginGetListing(new AsyncCallback(GetListingCallback), Global.conn);
            ////Global.conn.BeginSetWorkingDirectory("/Programmi/rotor_13/", new AsyncCallback(BeginSetWorkingDirectoryCallback), Global.conn);
            ////Global.conn.BeginGetListing(new AsyncCallback(GetListingCallback), Global.conn);
            ////string path = Global.ParStr.CreatePathFile("FileRicette");
            ////using (Stream istream = Global.conn.OpenRead("Prog.dat"))
            ////{
            ////    try
            ////    {
            ////        using (var fileStream = new FileStream(path + "\\Prog.dat", FileMode.Create, FileAccess.Write))
            ////        {
            ////            istream.CopyTo(fileStream);
            ////            fileStream.Close();
            ////        }
            ////        System.Diagnostics.Debug.WriteLine("Scaricato");
            ////        // istream.Position is incremented accordingly to the reads you perform
            ////        // istream.Length == file size if the server supports getting the file size
            ////        // also note that file size for the same file can vary between ASCII and Binary
            ////        // modes and some servers won't even give a file size for ASCII files! It is
            ////        // recommended that you stick with Binary and worry about character encodings
            ////        // on your end of the connection.

            ////    }
            ////    finally
            ////    {
            ////        System.Diagnostics.Debug.WriteLine("Finito");
            ////        istream.Close();

            ////    }
            ////}


        }

        private static string[] SplitString(string str)
        {
            List<string> allTokens = new List<string>(str.Split(null));
            for (int i = allTokens.Count - 1; i >= 0; i--)
                if (((string)allTokens[i]).Trim().Length == 0)
                    allTokens.RemoveAt(i);
            return (string[])allTokens.ToArray();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void informazioniSuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Informazioni frmInfo = new Informazioni();

            frmInfo.ShowDialog();
        }

        static void BeginGetWorkingDirectoryCallback(IAsyncResult ar)
        {
            FtpClient conn = ar.AsyncState as FtpClient;

            try
            {
                if (conn == null)
                    throw new InvalidOperationException("The FtpControlConnection object is null!");

                Console.WriteLine("Working directory: " + conn.EndGetWorkingDirectory(ar));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                m_reset.Set();
            }
        }

        static void GetListingCallback(IAsyncResult ar)
        {
            FtpClient conn = ar.AsyncState as FtpClient;

            try
            {
                if (conn == null)
                    throw new InvalidOperationException("The FtpControlConnection object is null!");

                foreach (FtpListItem item in conn.EndGetListing(ar))
                    Console.WriteLine(item);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                m_reset.Set();
            }
        }

        static void BeginSetWorkingDirectoryCallback(IAsyncResult ar)
        {
            FtpClient conn = ar.AsyncState as FtpClient;

            try
            {
                if (conn == null)
                    throw new InvalidOperationException("The FtpControlConnection object is null!");

                conn.EndSetWorkingDirectory(ar);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                m_reset.Set();
            }
        }

        /// <summary>
		/// A list of asynchronous methods that are in progress
		/// </summary>
		readonly Dictionary<IAsyncResult, object> m_asyncmethods = new Dictionary<IAsyncResult, object>();
        delegate void AsyncSetDownloadFiles(string[] path);

        /// <summary>
        /// Begins an asynchronous operation to set the working directory on the server
        /// </summary>
        /// <param name="path">The directory to change to</param>
        /// <param name="callback">Async Callback</param>
        /// <param name="state">State object</param>
        /// <returns>IAsyncResult</returns>
        /// <example><code source="..\Examples\BeginSetWorkingDirectory.cs" lang="cs" /></example>
        public IAsyncResult BeginDownloadFiles(string[] path, AsyncCallback callback, object state)
        {
            IAsyncResult ar;
            AsyncSetDownloadFiles func;

            lock (m_asyncmethods)
            {
                ar = (func = new AsyncSetDownloadFiles(DownloadFiles)).BeginInvoke(path, callback, state);
                m_asyncmethods.Add(ar, func);
            }

            return ar;
        }

        ///// <summary>
        ///// Ends a call to <see cref="BeginSetWorkingDirectory"/>
        ///// </summary>
        ///// <param name="ar">IAsyncResult returned from <see cref="BeginSetWorkingDirectory"/></param>
        ///// <example><code source="..\Examples\BeginSetWorkingDirectory.cs" lang="cs" /></example>
        //public void EndSetWorkingDirectory(IAsyncResult ar)
        //{
        //    GetAsyncDelegate<AsyncSetWorkingDirectory>(ar).EndInvoke(ar);
        //}

        public void DownloadFiles(string[] listaFileRemoti)
        {
            //lock (m_reset)
            //{
            //    using (FtpClient cl = NewFtpClient())
            //    {
            //        cl.DownloadFiles(@"C:\DatiFornoRotor\Immagini", listaFileRemoti, false);
            //        cl.Dispose();
            //    }
            //}
        }

        private void txtAddress_TextChanged(object sender, EventArgs e)
        {
            if (txtAddress.Text.Length > 0)
                btnConnect.Enabled = true;
            else
                btnConnect.Enabled = false;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Connectiondata conndata = new Connectiondata();
            try
            {
                conndata.address = txtAddress.Text;
                conndata.username = m_user;
                conndata.password = m_pass;
                conndata.port = m_port.ToString();
                
                ftpClient = NewFtpClient(txtAddress.Text, m_port, m_user, m_pass);
                ftpClient.Connect();

                btnConnect.Enabled = false;
                btnDisconnect.Enabled = true;
                groupBox1.BackColor = Color.Green;

                //Se la connessione va a buon fine la aggiungo alla lista delle connessioni buone
                if (!connectionlist.SearchItem(conndata))
                {
                    connectionlist.AddItem(conndata);
                }

                downloadRicetteToolStripMenuItem.Enabled = true;
                uploadRicetteToolStripMenuItem.Enabled = true;
                resettaDatabaseToolStripMenuItem.Enabled = false;

            }
            catch(System.Net.Sockets.SocketException se)
            {
                if (se.Message == "Host sconosciuto")
                    System.Diagnostics.Debug.WriteLine("HOST SCONOSCIUTOSSSS");

                ftpClient.Dispose();
            }
            catch (FtpException)
            {

            }
            catch
            {
                //ftpClient.Close();
                //ftpClient = null;
                btnConnect.Enabled = true;
                btnDisconnect.Enabled = false;
                //lvFiles.AllowDrop = false;
            }

        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            ftpClient.Dispose();
            btnConnect.Enabled = true;
            btnDisconnect.Enabled = false;
            groupBox1.BackColor = Color.Red;

            downloadRicetteToolStripMenuItem.Enabled = false;
            uploadRicetteToolStripMenuItem.Enabled = false;
            resettaDatabaseToolStripMenuItem.Enabled = true;
        }

        private void connessioniPrecedentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // show previous connections
            frmConnections fConnections = new frmConnections();

            if ((DialogResult)fConnections.ShowDialog(connectionlist) == DialogResult.OK)
            {
                Connectiondata conndata;

                conndata = connectionlist.Item(fConnections.iitemSelected);
                txtAddress.Text = conndata.address;
                m_user = conndata.username;
                m_pass = conndata.password;
                m_port = Convert.ToInt32(conndata.port);
    
                conndata = null;

            }

            fConnections = null;
        }

        private void collegaFornoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConnParameters fParConnection = new frmConnParameters();

            if ((DialogResult)fParConnection.ShowDialog() == DialogResult.OK)
            {
                //Connectiondata conndata;

                //conndata = connectionlist.Item(fParConnection.iitemSelected);
                //txtAddress.Text = conndata.address;
                m_user = fParConnection.m_user;
                m_pass = fParConnection.m_pass;
                m_port = Convert.ToInt32(fParConnection.m_port);

                //conndata = null;

            }

            fParConnection = null;
        }

        private void loadRicetteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool eccezione = false;

            if (ftpClient != null)
            {
                waitForm.Show(this);
                try
                {
                    ftpClient.SetWorkingDirectory("/NANDFlash/Rotor/");
                    string[] listaFile = ftpClient.GetNameListing("/NANDFlash/Rotor/");
                    if (listaFile.Contains("Prog.dat"))
                    {
                        ftpClient.DownloadFile(@"C:\DatiFornoRotor\Prog.dat", "/NANDFlash/Rotor/Prog.dat");
                        Global.ricette.ReadFile(@"C:\DatiFornoRotor\Prog.dat");
                    }

                    if (ftpClient.DirectoryExists("/NANDFlash/Rotor/Immagini/") == true)
                    {
                        ftpClient.SetWorkingDirectory("/NANDFlash/Rotor/Immagini/");
                        string[] listaImmagini = ftpClient.GetNameListing("/NANDFlash/Rotor/Immagini/");
                        ftpClient.DownloadFiles(@"C:\DatiFornoRotor\Immagini", listaImmagini, false);
                    }

                }
                catch (Exception)
                {
                    eccezione = true;
                }
                finally
                {
                    waitForm.Close();

                    if (eccezione == false)
                    {
                        frmProgram fpr = new frmProgram();
                        fpr.ShowDialog();
                    }
                }
                
            }
        }

      
        private void txtAddress_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnConnect_Click(this, new EventArgs());
            }
        }

        private void uploadRicetteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ftpClient != null)
            {
                waitForm.Show(this);
                ftpClient.SetWorkingDirectory("/NANDFlash/Rotor/");

                if (File.Exists(@"C:\DatiFornoRotor\Prog.dat"))
                    ftpClient.UploadFile(@"C:\DatiFornoRotor\Prog.dat", "/NANDFlash/Rotor/Prog.dat");

                waitForm.Close();
               
            }
        }

        private void resettaDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dir = @"C:\DatiFornoRotor";

            if (Directory.Exists(dir))
            {
                Directory.Delete(dir, true);
            }
        }
    }
}
