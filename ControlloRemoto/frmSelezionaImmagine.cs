﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ControlloRemoto
{
    public partial class frmSelezionaImmagine : Form
    {
        DialogResult azione = DialogResult.Cancel;

        public frmSelezionaImmagine()
        {
            InitializeComponent();

        }

        public object ShowDialog(ref ClProg prog)
        {
            flowLayoutPanel1.Controls.Clear();

            List<UserControl2> _itemImmagini = new List<UserControl2>();

            string[] nomifile = Directory.GetFiles(@"C:\DatiFornoRotor\Immagini");

            for (int i = 0; i < nomifile.Length; i++)
            {
                UserControl2 ucr = new UserControl2();
                ucr.ImgRic = Image.FromFile(nomifile[i]);//@"C:\DatiFornoRotor\Immagini\" + new String(Global.ricette.Prog[i].LeggiNomeFig(true, @"C:\DatiFornoRotor\Immagini\")));
                if(nomifile[i].Contains(new String(prog.LeggiNomeFig(true, @"C:\DatiFornoRotor\Immagini\"))))
                    ucr.BackColor = Color.LimeGreen;
                else
                    ucr.BackColor = Color.Black;

                _itemImmagini.Add(ucr);

            }

            for (int i = 0; i < _itemImmagini.Count; i++)
            {
                _itemImmagini[i].Click += FrmProgram_Click;
                flowLayoutPanel1.Controls.Add(_itemImmagini[i]);
            }

            base.ShowDialog();

            if(azione == DialogResult.OK)
            {
                int i = 0;
                for (i = 0; i < flowLayoutPanel1.Controls.Count; i++)
                {
                    if (flowLayoutPanel1.Controls[i].BackColor == Color.LimeGreen)
                        break;
                }

                prog.InserisciNomeFig(Path.GetFileNameWithoutExtension(nomifile[i]));
            }

            return azione;
        }

        private void FrmProgram_Click(object sender, EventArgs e)
        {

            int indice = ((UserControl2)sender).TabIndex;

            for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
            {
                if (i == indice)
                    flowLayoutPanel1.Controls[i].BackColor = Color.LimeGreen;
                else
                    flowLayoutPanel1.Controls[i].BackColor = Color.Black;
            }

           
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            azione = DialogResult.OK;

            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            azione = DialogResult.Cancel;

            this.Close();
        }
    }
}
