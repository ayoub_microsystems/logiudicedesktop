﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class UserControl2 : UserControl
    {
        private Image _img;

        public UserControl2()
        {
            InitializeComponent();
        }
        public Image ImgRic
        {
            get { return _img; }
            set
            {
                _img = value;
                pbImage.Image = _img;
            }
        }

        private void pbImage_Click(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, e);
        }
    }
}
