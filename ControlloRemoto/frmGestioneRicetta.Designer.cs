﻿namespace ControlloRemoto
{
    partial class frmGestioneRicetta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGestioneRicetta));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbImmagineRic = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txbNumFasi = new System.Windows.Forms.TextBox();
            this.txbRitardoOffVent = new System.Windows.Forms.TextBox();
            this.txbRitardoOnVent = new System.Windows.Forms.TextBox();
            this.txbRitardoOnBruciatore = new System.Windows.Forms.TextBox();
            this.btnModificaFasi = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txbNomeRic = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImmagineRic)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(186, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pbImmagineRic
            // 
            this.pbImmagineRic.Location = new System.Drawing.Point(128, 115);
            this.pbImmagineRic.Name = "pbImmagineRic";
            this.pbImmagineRic.Size = new System.Drawing.Size(240, 175);
            this.pbImmagineRic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbImmagineRic.TabIndex = 2;
            this.pbImmagineRic.TabStop = false;
            this.pbImmagineRic.Click += new System.EventHandler(this.pbImmagineRic_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel1.Location = new System.Drawing.Point(0, 380);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 5);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel2.Location = new System.Drawing.Point(0, 435);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(480, 5);
            this.panel2.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel3.Location = new System.Drawing.Point(0, 490);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(480, 5);
            this.panel3.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel4.Location = new System.Drawing.Point(0, 545);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(480, 5);
            this.panel4.TabIndex = 7;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel5.Location = new System.Drawing.Point(0, 600);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(480, 5);
            this.panel5.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 393);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 34);
            this.label2.TabIndex = 9;
            this.label2.Text = "NUMERO FASI";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 447);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(262, 34);
            this.label3.TabIndex = 10;
            this.label3.Text = "RITARDO OFF VENTOLE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 502);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(255, 34);
            this.label4.TabIndex = 11;
            this.label4.Text = "RITARDO ON VENTOLE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(0, 557);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(294, 34);
            this.label5.TabIndex = 12;
            this.label5.Text = "RITARDO ON BRUCIATORE";
            // 
            // txbNumFasi
            // 
            this.txbNumFasi.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNumFasi.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNumFasi.Location = new System.Drawing.Point(369, 390);
            this.txbNumFasi.Name = "txbNumFasi";
            this.txbNumFasi.Size = new System.Drawing.Size(100, 39);
            this.txbNumFasi.TabIndex = 13;
            this.txbNumFasi.Text = "1";
            this.txbNumFasi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoOffVent
            // 
            this.txbRitardoOffVent.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoOffVent.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoOffVent.Location = new System.Drawing.Point(369, 446);
            this.txbRitardoOffVent.Name = "txbRitardoOffVent";
            this.txbRitardoOffVent.Size = new System.Drawing.Size(100, 39);
            this.txbRitardoOffVent.TabIndex = 14;
            this.txbRitardoOffVent.Text = "0";
            this.txbRitardoOffVent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoOnVent
            // 
            this.txbRitardoOnVent.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoOnVent.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoOnVent.Location = new System.Drawing.Point(369, 500);
            this.txbRitardoOnVent.Name = "txbRitardoOnVent";
            this.txbRitardoOnVent.Size = new System.Drawing.Size(100, 39);
            this.txbRitardoOnVent.TabIndex = 15;
            this.txbRitardoOnVent.Text = "2";
            this.txbRitardoOnVent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoOnBruciatore
            // 
            this.txbRitardoOnBruciatore.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoOnBruciatore.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoOnBruciatore.Location = new System.Drawing.Point(369, 555);
            this.txbRitardoOnBruciatore.Name = "txbRitardoOnBruciatore";
            this.txbRitardoOnBruciatore.Size = new System.Drawing.Size(100, 39);
            this.txbRitardoOnBruciatore.TabIndex = 16;
            this.txbRitardoOnBruciatore.Text = "3";
            this.txbRitardoOnBruciatore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnModificaFasi
            // 
            this.btnModificaFasi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.btnModificaFasi.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificaFasi.ForeColor = System.Drawing.Color.Black;
            this.btnModificaFasi.Location = new System.Drawing.Point(170, 614);
            this.btnModificaFasi.Name = "btnModificaFasi";
            this.btnModificaFasi.Size = new System.Drawing.Size(140, 140);
            this.btnModificaFasi.TabIndex = 17;
            this.btnModificaFasi.Text = "Modifica fasi";
            this.btnModificaFasi.UseVisualStyleBackColor = false;
            this.btnModificaFasi.Click += new System.EventHandler(this.btnModificaFasi_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.btnSave.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(327, 614);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 140);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Salva";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.button1.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(13, 614);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 140);
            this.button1.TabIndex = 19;
            this.button1.Text = "Reset";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txbNomeRic
            // 
            this.txbNomeRic.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.txbNomeRic.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNomeRic.ForeColor = System.Drawing.Color.White;
            this.txbNomeRic.Location = new System.Drawing.Point(111, 314);
            this.txbNomeRic.MaxLength = 15;
            this.txbNomeRic.Name = "txbNomeRic";
            this.txbNomeRic.Size = new System.Drawing.Size(268, 41);
            this.txbNomeRic.TabIndex = 20;
            this.txbNomeRic.Text = "nome ricetta";
            this.txbNomeRic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbNomeRic.TextChanged += new System.EventHandler(this.txbNomeRic_TextChanged);
            // 
            // frmGestioneRicetta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(481, 762);
            this.Controls.Add(this.txbNomeRic);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnModificaFasi);
            this.Controls.Add(this.txbRitardoOnBruciatore);
            this.Controls.Add(this.txbRitardoOnVent);
            this.Controls.Add(this.txbRitardoOffVent);
            this.Controls.Add(this.txbNumFasi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pbImmagineRic);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGestioneRicetta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestione Ricetta";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImmagineRic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbImmagineRic;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbNumFasi;
        private System.Windows.Forms.TextBox txbRitardoOffVent;
        private System.Windows.Forms.TextBox txbRitardoOnVent;
        private System.Windows.Forms.TextBox txbRitardoOnBruciatore;
        private System.Windows.Forms.Button btnModificaFasi;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbNomeRic;
    }
}