﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class frmGestioneRicetta : Form
    {
        DialogResult azione = DialogResult.Cancel;

        ClProg progLoaded = null;

        public frmGestioneRicetta()
        {
            InitializeComponent();

            
        }

        public object ShowDialog(ref ClProg prog)
        {
            progLoaded = prog;

            pbImmagineRic.Image = Image.FromFile(@"C:\DatiFornoRotor\Immagini\" + new String(progLoaded.LeggiNomeFig(true, @"C:\DatiFornoRotor\Immagini\")));
            txbNomeRic.Text = new String(progLoaded.Nome);
            txbNumFasi.Text = progLoaded.Parprog.num_fase.ToString();
            txbRitardoOffVent.Text = progLoaded.Parprog.rit_off_ventola.ToString();
            txbRitardoOnVent.Text = progLoaded.Parprog.rit_start_ventola.ToString();
            txbRitardoOnBruciatore.Text = progLoaded.Parprog.rit_start_bruciatore.ToString();

            base.ShowDialog();

            return azione; 
        }

        private void btnModificaFasi_Click(object sender, EventArgs e)
        {
            frmGestioneFasi fgf = new frmGestioneFasi();

            progLoaded.Parprog.num_fase = Convert.ToByte(txbNumFasi.Text);

            if ((DialogResult)fgf.ShowDialog(ref progLoaded) == DialogResult.OK)
            {
                System.Diagnostics.Debug.WriteLine("salvato");
            }

            fgf.Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            progLoaded.InserisciNome(txbNomeRic.Text);
            progLoaded.Parprog.num_fase = Convert.ToByte(txbNumFasi.Text);
            progLoaded.Parprog.rit_off_ventola = Convert.ToInt16(txbRitardoOffVent.Text);
            progLoaded.Parprog.rit_start_ventola = Convert.ToInt16(txbRitardoOnVent.Text);
            progLoaded.Parprog.rit_start_bruciatore = Convert.ToInt16(txbRitardoOnBruciatore.Text);

            azione = DialogResult.Yes;

            this.Close();
        }


        private void txbNomeRic_TextChanged(object sender, EventArgs e)
        {

        }

        private void pbImmagineRic_Click(object sender, EventArgs e)
        {
            frmSelezionaImmagine simm = new frmSelezionaImmagine();

           if((DialogResult)simm.ShowDialog(ref progLoaded) == DialogResult.OK)
            {
                //Carico l'immagine della nuova foto
                pbImmagineRic.Image = Image.FromFile(@"C:\DatiFornoRotor\Immagini\" + new String(progLoaded.LeggiNomeFig(true, @"C:\DatiFornoRotor\Immagini\")));
            }


        }
    }
}
