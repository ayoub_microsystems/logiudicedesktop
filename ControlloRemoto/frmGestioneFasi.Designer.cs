﻿namespace ControlloRemoto
{
    partial class frmGestioneFasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGestioneFasi));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txbTemperaturaFase1 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase6 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola7 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase7 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola5 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola4 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap4 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi3 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi2 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap3 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap1 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola8 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi3 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi2 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi1 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi8 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi1 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi6 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi5 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi4 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi5 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap8 = new System.Windows.Forms.TextBox();
            this.txbTempoVap5 = new System.Windows.Forms.TextBox();
            this.txbTempoVap4 = new System.Windows.Forms.TextBox();
            this.txbTempoVap3 = new System.Windows.Forms.TextBox();
            this.txbTempoVap2 = new System.Windows.Forms.TextBox();
            this.txbTempoVap1 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi8 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi7 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi6 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola3 = new System.Windows.Forms.TextBox();
            this.txbTempoVap7 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap2 = new System.Windows.Forms.TextBox();
            this.txbPausaSbuffi7 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap7 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola2 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase5 = new System.Windows.Forms.TextBox();
            this.txbTempoVap6 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase3 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap6 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola1 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase4 = new System.Windows.Forms.TextBox();
            this.txbTempoVap8 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase8 = new System.Windows.Forms.TextBox();
            this.txbRitardoVap5 = new System.Windows.Forms.TextBox();
            this.txbTempoValvola6 = new System.Windows.Forms.TextBox();
            this.txbTemperaturaFase2 = new System.Windows.Forms.TextBox();
            this.txbNSbuffi4 = new System.Windows.Forms.TextBox();
            this.pnlMascheraFasi = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.txbVelocita8 = new System.Windows.Forms.TextBox();
            this.txbVelocita7 = new System.Windows.Forms.TextBox();
            this.txbVelocita6 = new System.Windows.Forms.TextBox();
            this.txbVelocita5 = new System.Windows.Forms.TextBox();
            this.txbVelocita4 = new System.Windows.Forms.TextBox();
            this.txbVelocita3 = new System.Windows.Forms.TextBox();
            this.txbVelocita2 = new System.Windows.Forms.TextBox();
            this.txbVelocita1 = new System.Windows.Forms.TextBox();
            this.pnlMascheraVelocita = new System.Windows.Forms.Panel();
            this.txbTempoCottura1 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura2 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura5 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura4 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura3 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura8 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura7 = new System.Windows.Forms.MaskedTextBox();
            this.txbTempoCottura6 = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(478, 88);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(388, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(482, 89);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(1, 92);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(89, 485);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel1.Location = new System.Drawing.Point(90, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(779, 3);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel2.Location = new System.Drawing.Point(90, 269);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(779, 3);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel3.Location = new System.Drawing.Point(90, 209);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(779, 3);
            this.panel3.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel4.Location = new System.Drawing.Point(90, 329);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(779, 3);
            this.panel4.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel5.Location = new System.Drawing.Point(91, 390);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(779, 3);
            this.panel5.TabIndex = 7;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel7.Location = new System.Drawing.Point(90, 449);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(779, 3);
            this.panel7.TabIndex = 8;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel8.Location = new System.Drawing.Point(90, 509);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(779, 3);
            this.panel8.TabIndex = 9;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel6.Location = new System.Drawing.Point(187, 89);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(3, 483);
            this.panel6.TabIndex = 10;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel9.Location = new System.Drawing.Point(284, 90);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(3, 483);
            this.panel9.TabIndex = 11;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel10.Location = new System.Drawing.Point(381, 89);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(3, 483);
            this.panel10.TabIndex = 12;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel11.Location = new System.Drawing.Point(478, 89);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(3, 483);
            this.panel11.TabIndex = 11;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel12.Location = new System.Drawing.Point(575, 89);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(3, 483);
            this.panel12.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel13.Location = new System.Drawing.Point(672, 89);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(3, 483);
            this.panel13.TabIndex = 14;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel14.Location = new System.Drawing.Point(769, 89);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(3, 483);
            this.panel14.TabIndex = 15;
            // 
            // txbTemperaturaFase1
            // 
            this.txbTemperaturaFase1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase1.Location = new System.Drawing.Point(98, 101);
            this.txbTemperaturaFase1.MaxLength = 999;
            this.txbTemperaturaFase1.Name = "txbTemperaturaFase1";
            this.txbTemperaturaFase1.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase1.TabIndex = 16;
            this.txbTemperaturaFase1.Text = "200";
            this.txbTemperaturaFase1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase6
            // 
            this.txbTemperaturaFase6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase6.Location = new System.Drawing.Point(583, 101);
            this.txbTemperaturaFase6.MaxLength = 999;
            this.txbTemperaturaFase6.Name = "txbTemperaturaFase6";
            this.txbTemperaturaFase6.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase6.TabIndex = 21;
            this.txbTemperaturaFase6.Text = "200";
            this.txbTemperaturaFase6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola7
            // 
            this.txbTempoValvola7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola7.Location = new System.Drawing.Point(679, 460);
            this.txbTempoValvola7.Name = "txbTempoValvola7";
            this.txbTempoValvola7.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola7.TabIndex = 70;
            this.txbTempoValvola7.Text = "0";
            this.txbTempoValvola7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase7
            // 
            this.txbTemperaturaFase7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase7.Location = new System.Drawing.Point(680, 101);
            this.txbTemperaturaFase7.MaxLength = 999;
            this.txbTemperaturaFase7.Name = "txbTemperaturaFase7";
            this.txbTemperaturaFase7.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase7.TabIndex = 22;
            this.txbTemperaturaFase7.Text = "200";
            this.txbTemperaturaFase7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola5
            // 
            this.txbTempoValvola5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola5.Location = new System.Drawing.Point(485, 460);
            this.txbTempoValvola5.Name = "txbTempoValvola5";
            this.txbTempoValvola5.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola5.TabIndex = 68;
            this.txbTempoValvola5.Text = "0";
            this.txbTempoValvola5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola4
            // 
            this.txbTempoValvola4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola4.Location = new System.Drawing.Point(388, 460);
            this.txbTempoValvola4.Name = "txbTempoValvola4";
            this.txbTempoValvola4.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola4.TabIndex = 67;
            this.txbTempoValvola4.Text = "0";
            this.txbTempoValvola4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap4
            // 
            this.txbRitardoVap4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap4.Location = new System.Drawing.Point(388, 401);
            this.txbRitardoVap4.Name = "txbRitardoVap4";
            this.txbRitardoVap4.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap4.TabIndex = 59;
            this.txbRitardoVap4.Text = "0";
            this.txbRitardoVap4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi3
            // 
            this.txbPausaSbuffi3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi3.Location = new System.Drawing.Point(292, 341);
            this.txbPausaSbuffi3.MaxLength = 999;
            this.txbPausaSbuffi3.Name = "txbPausaSbuffi3";
            this.txbPausaSbuffi3.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi3.TabIndex = 50;
            this.txbPausaSbuffi3.Text = "0";
            this.txbPausaSbuffi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi2
            // 
            this.txbPausaSbuffi2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi2.Location = new System.Drawing.Point(195, 341);
            this.txbPausaSbuffi2.MaxLength = 999;
            this.txbPausaSbuffi2.Name = "txbPausaSbuffi2";
            this.txbPausaSbuffi2.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi2.TabIndex = 49;
            this.txbPausaSbuffi2.Text = "0";
            this.txbPausaSbuffi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap3
            // 
            this.txbRitardoVap3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap3.Location = new System.Drawing.Point(291, 401);
            this.txbRitardoVap3.Name = "txbRitardoVap3";
            this.txbRitardoVap3.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap3.TabIndex = 58;
            this.txbRitardoVap3.Text = "0";
            this.txbRitardoVap3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap1
            // 
            this.txbRitardoVap1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap1.Location = new System.Drawing.Point(97, 401);
            this.txbRitardoVap1.Name = "txbRitardoVap1";
            this.txbRitardoVap1.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap1.TabIndex = 56;
            this.txbRitardoVap1.Text = "0";
            this.txbRitardoVap1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola8
            // 
            this.txbTempoValvola8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola8.Location = new System.Drawing.Point(776, 460);
            this.txbTempoValvola8.Name = "txbTempoValvola8";
            this.txbTempoValvola8.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola8.TabIndex = 71;
            this.txbTempoValvola8.Text = "0";
            this.txbTempoValvola8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi3
            // 
            this.txbNSbuffi3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi3.Location = new System.Drawing.Point(292, 280);
            this.txbNSbuffi3.MaxLength = 9;
            this.txbNSbuffi3.Name = "txbNSbuffi3";
            this.txbNSbuffi3.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi3.TabIndex = 42;
            this.txbNSbuffi3.Text = "0";
            this.txbNSbuffi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi2
            // 
            this.txbNSbuffi2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi2.Location = new System.Drawing.Point(195, 280);
            this.txbNSbuffi2.MaxLength = 9;
            this.txbNSbuffi2.Name = "txbNSbuffi2";
            this.txbNSbuffi2.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi2.TabIndex = 41;
            this.txbNSbuffi2.Text = "0";
            this.txbNSbuffi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi1
            // 
            this.txbNSbuffi1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi1.Location = new System.Drawing.Point(98, 280);
            this.txbNSbuffi1.MaxLength = 10;
            this.txbNSbuffi1.Name = "txbNSbuffi1";
            this.txbNSbuffi1.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi1.TabIndex = 40;
            this.txbNSbuffi1.Text = "0";
            this.txbNSbuffi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbNSbuffi1.TextChanged += new System.EventHandler(this.txbNSbuffi1_TextChanged);
            // 
            // txbPausaSbuffi8
            // 
            this.txbPausaSbuffi8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi8.Location = new System.Drawing.Point(777, 341);
            this.txbPausaSbuffi8.MaxLength = 999;
            this.txbPausaSbuffi8.Name = "txbPausaSbuffi8";
            this.txbPausaSbuffi8.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi8.TabIndex = 55;
            this.txbPausaSbuffi8.Text = "0";
            this.txbPausaSbuffi8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi1
            // 
            this.txbPausaSbuffi1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi1.Location = new System.Drawing.Point(98, 341);
            this.txbPausaSbuffi1.MaxLength = 999;
            this.txbPausaSbuffi1.Name = "txbPausaSbuffi1";
            this.txbPausaSbuffi1.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi1.TabIndex = 48;
            this.txbPausaSbuffi1.Text = "0";
            this.txbPausaSbuffi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbPausaSbuffi1.TextChanged += new System.EventHandler(this.txbPausaSbuffi1_TextChanged);
            // 
            // txbPausaSbuffi6
            // 
            this.txbPausaSbuffi6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi6.Location = new System.Drawing.Point(583, 341);
            this.txbPausaSbuffi6.MaxLength = 999;
            this.txbPausaSbuffi6.Name = "txbPausaSbuffi6";
            this.txbPausaSbuffi6.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi6.TabIndex = 53;
            this.txbPausaSbuffi6.Text = "0";
            this.txbPausaSbuffi6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi5
            // 
            this.txbPausaSbuffi5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi5.Location = new System.Drawing.Point(486, 341);
            this.txbPausaSbuffi5.MaxLength = 999;
            this.txbPausaSbuffi5.Name = "txbPausaSbuffi5";
            this.txbPausaSbuffi5.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi5.TabIndex = 52;
            this.txbPausaSbuffi5.Text = "0";
            this.txbPausaSbuffi5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi4
            // 
            this.txbPausaSbuffi4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi4.Location = new System.Drawing.Point(389, 341);
            this.txbPausaSbuffi4.MaxLength = 999;
            this.txbPausaSbuffi4.Name = "txbPausaSbuffi4";
            this.txbPausaSbuffi4.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi4.TabIndex = 51;
            this.txbPausaSbuffi4.Text = "0";
            this.txbPausaSbuffi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi5
            // 
            this.txbNSbuffi5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi5.Location = new System.Drawing.Point(486, 280);
            this.txbNSbuffi5.MaxLength = 9;
            this.txbNSbuffi5.Name = "txbNSbuffi5";
            this.txbNSbuffi5.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi5.TabIndex = 44;
            this.txbNSbuffi5.Text = "0";
            this.txbNSbuffi5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap8
            // 
            this.txbRitardoVap8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap8.Location = new System.Drawing.Point(776, 401);
            this.txbRitardoVap8.Name = "txbRitardoVap8";
            this.txbRitardoVap8.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap8.TabIndex = 63;
            this.txbRitardoVap8.Text = "0";
            this.txbRitardoVap8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap5
            // 
            this.txbTempoVap5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap5.Location = new System.Drawing.Point(486, 220);
            this.txbTempoVap5.MaxLength = 10;
            this.txbTempoVap5.Name = "txbTempoVap5";
            this.txbTempoVap5.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap5.TabIndex = 36;
            this.txbTempoVap5.Text = "0";
            this.txbTempoVap5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap4
            // 
            this.txbTempoVap4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap4.Location = new System.Drawing.Point(389, 220);
            this.txbTempoVap4.MaxLength = 10;
            this.txbTempoVap4.Name = "txbTempoVap4";
            this.txbTempoVap4.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap4.TabIndex = 35;
            this.txbTempoVap4.Text = "0";
            this.txbTempoVap4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap3
            // 
            this.txbTempoVap3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap3.Location = new System.Drawing.Point(292, 220);
            this.txbTempoVap3.MaxLength = 10;
            this.txbTempoVap3.Name = "txbTempoVap3";
            this.txbTempoVap3.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap3.TabIndex = 34;
            this.txbTempoVap3.Text = "0";
            this.txbTempoVap3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap2
            // 
            this.txbTempoVap2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap2.Location = new System.Drawing.Point(195, 220);
            this.txbTempoVap2.MaxLength = 10;
            this.txbTempoVap2.Name = "txbTempoVap2";
            this.txbTempoVap2.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap2.TabIndex = 33;
            this.txbTempoVap2.Text = "0";
            this.txbTempoVap2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap1
            // 
            this.txbTempoVap1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap1.Location = new System.Drawing.Point(98, 220);
            this.txbTempoVap1.MaxLength = 10;
            this.txbTempoVap1.Name = "txbTempoVap1";
            this.txbTempoVap1.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap1.TabIndex = 32;
            this.txbTempoVap1.Text = "0";
            this.txbTempoVap1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoVap1.TextChanged += new System.EventHandler(this.txbTempoVap1_TextChanged);
            // 
            // txbNSbuffi8
            // 
            this.txbNSbuffi8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi8.Location = new System.Drawing.Point(777, 280);
            this.txbNSbuffi8.MaxLength = 9;
            this.txbNSbuffi8.Name = "txbNSbuffi8";
            this.txbNSbuffi8.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi8.TabIndex = 47;
            this.txbNSbuffi8.Text = "0";
            this.txbNSbuffi8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi7
            // 
            this.txbNSbuffi7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi7.Location = new System.Drawing.Point(680, 280);
            this.txbNSbuffi7.MaxLength = 9;
            this.txbNSbuffi7.Name = "txbNSbuffi7";
            this.txbNSbuffi7.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi7.TabIndex = 46;
            this.txbNSbuffi7.Text = "0";
            this.txbNSbuffi7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi6
            // 
            this.txbNSbuffi6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi6.Location = new System.Drawing.Point(583, 280);
            this.txbNSbuffi6.MaxLength = 9;
            this.txbNSbuffi6.Name = "txbNSbuffi6";
            this.txbNSbuffi6.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi6.TabIndex = 45;
            this.txbNSbuffi6.Text = "0";
            this.txbNSbuffi6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola3
            // 
            this.txbTempoValvola3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola3.Location = new System.Drawing.Point(291, 460);
            this.txbTempoValvola3.Name = "txbTempoValvola3";
            this.txbTempoValvola3.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola3.TabIndex = 66;
            this.txbTempoValvola3.Text = "0";
            this.txbTempoValvola3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap7
            // 
            this.txbTempoVap7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap7.Location = new System.Drawing.Point(680, 220);
            this.txbTempoVap7.MaxLength = 10;
            this.txbTempoVap7.Name = "txbTempoVap7";
            this.txbTempoVap7.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap7.TabIndex = 38;
            this.txbTempoVap7.Text = "0";
            this.txbTempoVap7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap2
            // 
            this.txbRitardoVap2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap2.Location = new System.Drawing.Point(194, 401);
            this.txbRitardoVap2.Name = "txbRitardoVap2";
            this.txbRitardoVap2.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap2.TabIndex = 57;
            this.txbRitardoVap2.Text = "0";
            this.txbRitardoVap2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbPausaSbuffi7
            // 
            this.txbPausaSbuffi7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbPausaSbuffi7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbPausaSbuffi7.Location = new System.Drawing.Point(680, 341);
            this.txbPausaSbuffi7.MaxLength = 999;
            this.txbPausaSbuffi7.Name = "txbPausaSbuffi7";
            this.txbPausaSbuffi7.Size = new System.Drawing.Size(85, 39);
            this.txbPausaSbuffi7.TabIndex = 54;
            this.txbPausaSbuffi7.Text = "0";
            this.txbPausaSbuffi7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap7
            // 
            this.txbRitardoVap7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap7.Location = new System.Drawing.Point(679, 401);
            this.txbRitardoVap7.Name = "txbRitardoVap7";
            this.txbRitardoVap7.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap7.TabIndex = 62;
            this.txbRitardoVap7.Text = "0";
            this.txbRitardoVap7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola2
            // 
            this.txbTempoValvola2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola2.Location = new System.Drawing.Point(194, 460);
            this.txbTempoValvola2.Name = "txbTempoValvola2";
            this.txbTempoValvola2.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola2.TabIndex = 65;
            this.txbTempoValvola2.Text = "0";
            this.txbTempoValvola2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase5
            // 
            this.txbTemperaturaFase5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase5.Location = new System.Drawing.Point(486, 101);
            this.txbTemperaturaFase5.MaxLength = 999;
            this.txbTemperaturaFase5.Name = "txbTemperaturaFase5";
            this.txbTemperaturaFase5.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase5.TabIndex = 20;
            this.txbTemperaturaFase5.Text = "200";
            this.txbTemperaturaFase5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap6
            // 
            this.txbTempoVap6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap6.Location = new System.Drawing.Point(583, 220);
            this.txbTempoVap6.MaxLength = 10;
            this.txbTempoVap6.Name = "txbTempoVap6";
            this.txbTempoVap6.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap6.TabIndex = 37;
            this.txbTempoVap6.Text = "0";
            this.txbTempoVap6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase3
            // 
            this.txbTemperaturaFase3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase3.Location = new System.Drawing.Point(292, 101);
            this.txbTemperaturaFase3.MaxLength = 999;
            this.txbTemperaturaFase3.Name = "txbTemperaturaFase3";
            this.txbTemperaturaFase3.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase3.TabIndex = 18;
            this.txbTemperaturaFase3.Text = "200";
            this.txbTemperaturaFase3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap6
            // 
            this.txbRitardoVap6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap6.Location = new System.Drawing.Point(582, 401);
            this.txbRitardoVap6.Name = "txbRitardoVap6";
            this.txbRitardoVap6.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap6.TabIndex = 61;
            this.txbRitardoVap6.Text = "0";
            this.txbRitardoVap6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola1
            // 
            this.txbTempoValvola1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola1.Location = new System.Drawing.Point(97, 460);
            this.txbTempoValvola1.Name = "txbTempoValvola1";
            this.txbTempoValvola1.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola1.TabIndex = 64;
            this.txbTempoValvola1.Text = "0";
            this.txbTempoValvola1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase4
            // 
            this.txbTemperaturaFase4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase4.Location = new System.Drawing.Point(389, 101);
            this.txbTemperaturaFase4.MaxLength = 999;
            this.txbTemperaturaFase4.Name = "txbTemperaturaFase4";
            this.txbTemperaturaFase4.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase4.TabIndex = 19;
            this.txbTemperaturaFase4.Text = "200";
            this.txbTemperaturaFase4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoVap8
            // 
            this.txbTempoVap8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoVap8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoVap8.Location = new System.Drawing.Point(777, 220);
            this.txbTempoVap8.MaxLength = 10;
            this.txbTempoVap8.Name = "txbTempoVap8";
            this.txbTempoVap8.Size = new System.Drawing.Size(85, 39);
            this.txbTempoVap8.TabIndex = 39;
            this.txbTempoVap8.Text = "0";
            this.txbTempoVap8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase8
            // 
            this.txbTemperaturaFase8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase8.Location = new System.Drawing.Point(777, 101);
            this.txbTemperaturaFase8.MaxLength = 999;
            this.txbTemperaturaFase8.Name = "txbTemperaturaFase8";
            this.txbTemperaturaFase8.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase8.TabIndex = 23;
            this.txbTemperaturaFase8.Text = "200";
            this.txbTemperaturaFase8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbRitardoVap5
            // 
            this.txbRitardoVap5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbRitardoVap5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRitardoVap5.Location = new System.Drawing.Point(485, 401);
            this.txbRitardoVap5.Name = "txbRitardoVap5";
            this.txbRitardoVap5.Size = new System.Drawing.Size(85, 39);
            this.txbRitardoVap5.TabIndex = 60;
            this.txbRitardoVap5.Text = "0";
            this.txbRitardoVap5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTempoValvola6
            // 
            this.txbTempoValvola6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoValvola6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoValvola6.Location = new System.Drawing.Point(582, 460);
            this.txbTempoValvola6.Name = "txbTempoValvola6";
            this.txbTempoValvola6.Size = new System.Drawing.Size(85, 39);
            this.txbTempoValvola6.TabIndex = 69;
            this.txbTempoValvola6.Text = "0";
            this.txbTempoValvola6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbTemperaturaFase2
            // 
            this.txbTemperaturaFase2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTemperaturaFase2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTemperaturaFase2.Location = new System.Drawing.Point(195, 101);
            this.txbTemperaturaFase2.MaxLength = 999;
            this.txbTemperaturaFase2.Name = "txbTemperaturaFase2";
            this.txbTemperaturaFase2.Size = new System.Drawing.Size(85, 39);
            this.txbTemperaturaFase2.TabIndex = 17;
            this.txbTemperaturaFase2.Text = "200";
            this.txbTemperaturaFase2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbNSbuffi4
            // 
            this.txbNSbuffi4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbNSbuffi4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNSbuffi4.Location = new System.Drawing.Point(389, 280);
            this.txbNSbuffi4.MaxLength = 9;
            this.txbNSbuffi4.Name = "txbNSbuffi4";
            this.txbNSbuffi4.Size = new System.Drawing.Size(85, 39);
            this.txbNSbuffi4.TabIndex = 43;
            this.txbNSbuffi4.Text = "0";
            this.txbNSbuffi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlMascheraFasi
            // 
            this.pnlMascheraFasi.Location = new System.Drawing.Point(825, 8);
            this.pnlMascheraFasi.Name = "pnlMascheraFasi";
            this.pnlMascheraFasi.Size = new System.Drawing.Size(682, 569);
            this.pnlMascheraFasi.TabIndex = 72;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.btnReset.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(141, 589);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(189, 93);
            this.btnReset.TabIndex = 74;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.btnOk.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.Black;
            this.btnOk.Location = new System.Drawing.Point(545, 589);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(189, 93);
            this.btnOk.TabIndex = 73;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(150)))), ((int)(((byte)(11)))));
            this.panel16.Location = new System.Drawing.Point(90, 569);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(779, 3);
            this.panel16.TabIndex = 75;
            // 
            // txbVelocita8
            // 
            this.txbVelocita8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita8.Location = new System.Drawing.Point(775, 521);
            this.txbVelocita8.MaxLength = 1;
            this.txbVelocita8.Name = "txbVelocita8";
            this.txbVelocita8.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita8.TabIndex = 83;
            this.txbVelocita8.Text = "0";
            this.txbVelocita8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita7
            // 
            this.txbVelocita7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita7.Location = new System.Drawing.Point(678, 521);
            this.txbVelocita7.MaxLength = 1;
            this.txbVelocita7.Name = "txbVelocita7";
            this.txbVelocita7.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita7.TabIndex = 82;
            this.txbVelocita7.Text = "0";
            this.txbVelocita7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita6
            // 
            this.txbVelocita6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita6.Location = new System.Drawing.Point(581, 521);
            this.txbVelocita6.MaxLength = 1;
            this.txbVelocita6.Name = "txbVelocita6";
            this.txbVelocita6.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita6.TabIndex = 81;
            this.txbVelocita6.Text = "0";
            this.txbVelocita6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita5
            // 
            this.txbVelocita5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita5.Location = new System.Drawing.Point(484, 521);
            this.txbVelocita5.MaxLength = 1;
            this.txbVelocita5.Name = "txbVelocita5";
            this.txbVelocita5.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita5.TabIndex = 80;
            this.txbVelocita5.Text = "0";
            this.txbVelocita5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita4
            // 
            this.txbVelocita4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita4.Location = new System.Drawing.Point(387, 521);
            this.txbVelocita4.MaxLength = 1;
            this.txbVelocita4.Name = "txbVelocita4";
            this.txbVelocita4.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita4.TabIndex = 79;
            this.txbVelocita4.Text = "0";
            this.txbVelocita4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita3
            // 
            this.txbVelocita3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita3.Location = new System.Drawing.Point(290, 521);
            this.txbVelocita3.MaxLength = 1;
            this.txbVelocita3.Name = "txbVelocita3";
            this.txbVelocita3.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita3.TabIndex = 78;
            this.txbVelocita3.Text = "0";
            this.txbVelocita3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita2
            // 
            this.txbVelocita2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita2.Location = new System.Drawing.Point(193, 521);
            this.txbVelocita2.MaxLength = 1;
            this.txbVelocita2.Name = "txbVelocita2";
            this.txbVelocita2.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita2.TabIndex = 77;
            this.txbVelocita2.Text = "0";
            this.txbVelocita2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbVelocita1
            // 
            this.txbVelocita1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbVelocita1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbVelocita1.Location = new System.Drawing.Point(96, 521);
            this.txbVelocita1.MaxLength = 1;
            this.txbVelocita1.Name = "txbVelocita1";
            this.txbVelocita1.Size = new System.Drawing.Size(85, 39);
            this.txbVelocita1.TabIndex = 76;
            this.txbVelocita1.Text = "0";
            this.txbVelocita1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlMascheraVelocita
            // 
            this.pnlMascheraVelocita.Location = new System.Drawing.Point(1, 530);
            this.pnlMascheraVelocita.Name = "pnlMascheraVelocita";
            this.pnlMascheraVelocita.Size = new System.Drawing.Size(879, 65);
            this.pnlMascheraVelocita.TabIndex = 84;
            // 
            // txbTempoCottura1
            // 
            this.txbTempoCottura1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura1.Location = new System.Drawing.Point(98, 161);
            this.txbTempoCottura1.Mask = "00:00";
            this.txbTempoCottura1.Name = "txbTempoCottura1";
            this.txbTempoCottura1.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura1.TabIndex = 85;
            this.txbTempoCottura1.Text = "0000";
            this.txbTempoCottura1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura1.ValidatingType = typeof(System.DateTime);
            this.txbTempoCottura1.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            this.txbTempoCottura1.TextChanged += new System.EventHandler(this.maskedTextBox1_TextChanged);
            // 
            // txbTempoCottura2
            // 
            this.txbTempoCottura2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura2.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura2.Location = new System.Drawing.Point(195, 161);
            this.txbTempoCottura2.Mask = "00:00";
            this.txbTempoCottura2.Name = "txbTempoCottura2";
            this.txbTempoCottura2.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura2.TabIndex = 86;
            this.txbTempoCottura2.Text = "0000";
            this.txbTempoCottura2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura2.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura5
            // 
            this.txbTempoCottura5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura5.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura5.Location = new System.Drawing.Point(486, 161);
            this.txbTempoCottura5.Mask = "00:00";
            this.txbTempoCottura5.Name = "txbTempoCottura5";
            this.txbTempoCottura5.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura5.TabIndex = 87;
            this.txbTempoCottura5.Text = "0000";
            this.txbTempoCottura5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura5.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura4
            // 
            this.txbTempoCottura4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura4.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura4.Location = new System.Drawing.Point(389, 161);
            this.txbTempoCottura4.Mask = "00:00";
            this.txbTempoCottura4.Name = "txbTempoCottura4";
            this.txbTempoCottura4.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura4.TabIndex = 88;
            this.txbTempoCottura4.Text = "0000";
            this.txbTempoCottura4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura4.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura3
            // 
            this.txbTempoCottura3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura3.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura3.Location = new System.Drawing.Point(292, 161);
            this.txbTempoCottura3.Mask = "00:00";
            this.txbTempoCottura3.Name = "txbTempoCottura3";
            this.txbTempoCottura3.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura3.TabIndex = 89;
            this.txbTempoCottura3.Text = "0000";
            this.txbTempoCottura3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura3.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura8
            // 
            this.txbTempoCottura8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura8.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura8.Location = new System.Drawing.Point(777, 161);
            this.txbTempoCottura8.Mask = "00:00";
            this.txbTempoCottura8.Name = "txbTempoCottura8";
            this.txbTempoCottura8.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura8.TabIndex = 90;
            this.txbTempoCottura8.Text = "0000";
            this.txbTempoCottura8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura8.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura7
            // 
            this.txbTempoCottura7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura7.Location = new System.Drawing.Point(680, 161);
            this.txbTempoCottura7.Mask = "00:00";
            this.txbTempoCottura7.Name = "txbTempoCottura7";
            this.txbTempoCottura7.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura7.TabIndex = 91;
            this.txbTempoCottura7.Text = "0000";
            this.txbTempoCottura7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura7.ValidatingType = typeof(System.DateTime);
            // 
            // txbTempoCottura6
            // 
            this.txbTempoCottura6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txbTempoCottura6.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbTempoCottura6.Location = new System.Drawing.Point(583, 161);
            this.txbTempoCottura6.Mask = "00:00";
            this.txbTempoCottura6.Name = "txbTempoCottura6";
            this.txbTempoCottura6.Size = new System.Drawing.Size(85, 39);
            this.txbTempoCottura6.TabIndex = 92;
            this.txbTempoCottura6.Text = "0000";
            this.txbTempoCottura6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbTempoCottura6.ValidatingType = typeof(System.DateTime);
            // 
            // frmGestioneFasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(870, 690);
            this.Controls.Add(this.pnlMascheraFasi);
            this.Controls.Add(this.txbTempoCottura6);
            this.Controls.Add(this.txbTempoCottura7);
            this.Controls.Add(this.txbTempoCottura8);
            this.Controls.Add(this.txbTempoCottura3);
            this.Controls.Add(this.txbTempoCottura4);
            this.Controls.Add(this.txbTempoCottura5);
            this.Controls.Add(this.txbTempoCottura2);
            this.Controls.Add(this.txbTempoCottura1);
            this.Controls.Add(this.pnlMascheraVelocita);
            this.Controls.Add(this.txbVelocita8);
            this.Controls.Add(this.txbVelocita7);
            this.Controls.Add(this.txbVelocita6);
            this.Controls.Add(this.txbVelocita5);
            this.Controls.Add(this.txbVelocita4);
            this.Controls.Add(this.txbVelocita3);
            this.Controls.Add(this.txbVelocita2);
            this.Controls.Add(this.txbVelocita1);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txbTempoValvola8);
            this.Controls.Add(this.txbTempoValvola7);
            this.Controls.Add(this.txbTempoValvola6);
            this.Controls.Add(this.txbTempoValvola5);
            this.Controls.Add(this.txbTempoValvola4);
            this.Controls.Add(this.txbTempoValvola3);
            this.Controls.Add(this.txbTempoValvola2);
            this.Controls.Add(this.txbTempoValvola1);
            this.Controls.Add(this.txbRitardoVap8);
            this.Controls.Add(this.txbRitardoVap7);
            this.Controls.Add(this.txbRitardoVap6);
            this.Controls.Add(this.txbRitardoVap5);
            this.Controls.Add(this.txbRitardoVap4);
            this.Controls.Add(this.txbRitardoVap3);
            this.Controls.Add(this.txbRitardoVap2);
            this.Controls.Add(this.txbRitardoVap1);
            this.Controls.Add(this.txbPausaSbuffi8);
            this.Controls.Add(this.txbPausaSbuffi7);
            this.Controls.Add(this.txbPausaSbuffi6);
            this.Controls.Add(this.txbPausaSbuffi5);
            this.Controls.Add(this.txbPausaSbuffi4);
            this.Controls.Add(this.txbPausaSbuffi3);
            this.Controls.Add(this.txbPausaSbuffi2);
            this.Controls.Add(this.txbPausaSbuffi1);
            this.Controls.Add(this.txbNSbuffi8);
            this.Controls.Add(this.txbNSbuffi7);
            this.Controls.Add(this.txbNSbuffi6);
            this.Controls.Add(this.txbNSbuffi5);
            this.Controls.Add(this.txbNSbuffi4);
            this.Controls.Add(this.txbNSbuffi3);
            this.Controls.Add(this.txbNSbuffi2);
            this.Controls.Add(this.txbNSbuffi1);
            this.Controls.Add(this.txbTempoVap8);
            this.Controls.Add(this.txbTempoVap7);
            this.Controls.Add(this.txbTempoVap6);
            this.Controls.Add(this.txbTempoVap5);
            this.Controls.Add(this.txbTempoVap4);
            this.Controls.Add(this.txbTempoVap3);
            this.Controls.Add(this.txbTempoVap2);
            this.Controls.Add(this.txbTempoVap1);
            this.Controls.Add(this.txbTemperaturaFase8);
            this.Controls.Add(this.txbTemperaturaFase7);
            this.Controls.Add(this.txbTemperaturaFase6);
            this.Controls.Add(this.txbTemperaturaFase5);
            this.Controls.Add(this.txbTemperaturaFase4);
            this.Controls.Add(this.txbTemperaturaFase3);
            this.Controls.Add(this.txbTemperaturaFase2);
            this.Controls.Add(this.txbTemperaturaFase1);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGestioneFasi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestione fasi";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox txbTemperaturaFase1;
        private System.Windows.Forms.TextBox txbTemperaturaFase6;
        private System.Windows.Forms.TextBox txbTempoValvola7;
        private System.Windows.Forms.TextBox txbTemperaturaFase7;
        private System.Windows.Forms.TextBox txbTempoValvola5;
        private System.Windows.Forms.TextBox txbTempoValvola4;
        private System.Windows.Forms.TextBox txbRitardoVap4;
        private System.Windows.Forms.TextBox txbPausaSbuffi3;
        private System.Windows.Forms.TextBox txbPausaSbuffi2;
        private System.Windows.Forms.TextBox txbRitardoVap3;
        private System.Windows.Forms.TextBox txbRitardoVap1;
        private System.Windows.Forms.TextBox txbTempoValvola8;
        private System.Windows.Forms.TextBox txbNSbuffi3;
        private System.Windows.Forms.TextBox txbNSbuffi2;
        private System.Windows.Forms.TextBox txbNSbuffi1;
        private System.Windows.Forms.TextBox txbPausaSbuffi8;
        private System.Windows.Forms.TextBox txbPausaSbuffi1;
        private System.Windows.Forms.TextBox txbPausaSbuffi6;
        private System.Windows.Forms.TextBox txbPausaSbuffi5;
        private System.Windows.Forms.TextBox txbPausaSbuffi4;
        private System.Windows.Forms.TextBox txbNSbuffi5;
        private System.Windows.Forms.TextBox txbRitardoVap8;
        private System.Windows.Forms.TextBox txbTempoVap5;
        private System.Windows.Forms.TextBox txbTempoVap4;
        private System.Windows.Forms.TextBox txbTempoVap3;
        private System.Windows.Forms.TextBox txbTempoVap2;
        private System.Windows.Forms.TextBox txbTempoVap1;
        private System.Windows.Forms.TextBox txbNSbuffi8;
        private System.Windows.Forms.TextBox txbNSbuffi7;
        private System.Windows.Forms.TextBox txbNSbuffi6;
        private System.Windows.Forms.TextBox txbTempoValvola3;
        private System.Windows.Forms.TextBox txbTempoVap7;
        private System.Windows.Forms.TextBox txbRitardoVap2;
        private System.Windows.Forms.TextBox txbPausaSbuffi7;
        private System.Windows.Forms.TextBox txbRitardoVap7;
        private System.Windows.Forms.TextBox txbTempoValvola2;
        private System.Windows.Forms.TextBox txbTemperaturaFase5;
        private System.Windows.Forms.TextBox txbTempoVap6;
        private System.Windows.Forms.TextBox txbTemperaturaFase3;
        private System.Windows.Forms.TextBox txbRitardoVap6;
        private System.Windows.Forms.TextBox txbTempoValvola1;
        private System.Windows.Forms.TextBox txbTemperaturaFase4;
        private System.Windows.Forms.TextBox txbTempoVap8;
        private System.Windows.Forms.TextBox txbTemperaturaFase8;
        private System.Windows.Forms.TextBox txbRitardoVap5;
        private System.Windows.Forms.TextBox txbTempoValvola6;
        private System.Windows.Forms.TextBox txbTemperaturaFase2;
        private System.Windows.Forms.TextBox txbNSbuffi4;
        private System.Windows.Forms.Panel pnlMascheraFasi;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox txbVelocita8;
        private System.Windows.Forms.TextBox txbVelocita7;
        private System.Windows.Forms.TextBox txbVelocita6;
        private System.Windows.Forms.TextBox txbVelocita5;
        private System.Windows.Forms.TextBox txbVelocita4;
        private System.Windows.Forms.TextBox txbVelocita3;
        private System.Windows.Forms.TextBox txbVelocita2;
        private System.Windows.Forms.TextBox txbVelocita1;
        private System.Windows.Forms.Panel pnlMascheraVelocita;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura1;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura2;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura5;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura4;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura3;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura8;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura7;
        private System.Windows.Forms.MaskedTextBox txbTempoCottura6;
    }
}