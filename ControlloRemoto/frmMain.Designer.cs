﻿namespace ControlloRemoto
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.FileMenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadRicetteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadRicetteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collegaFornoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connessioniPrecedentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.informazioniSuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.resettaDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.FileMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(90, 289);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(290, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // FileMenuStrip1
            // 
            this.FileMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.forniToolStripMenuItem,
            this.toolStripMenuItem1});
            this.FileMenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.FileMenuStrip1.Name = "FileMenuStrip1";
            this.FileMenuStrip1.Size = new System.Drawing.Size(474, 24);
            this.FileMenuStrip1.TabIndex = 2;
            this.FileMenuStrip1.Text = "File";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadRicetteToolStripMenuItem,
            this.uploadRicetteToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // downloadRicetteToolStripMenuItem
            // 
            this.downloadRicetteToolStripMenuItem.Enabled = false;
            this.downloadRicetteToolStripMenuItem.Name = "downloadRicetteToolStripMenuItem";
            this.downloadRicetteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.downloadRicetteToolStripMenuItem.Text = "Download ricette";
            this.downloadRicetteToolStripMenuItem.Click += new System.EventHandler(this.loadRicetteToolStripMenuItem_Click);
            // 
            // uploadRicetteToolStripMenuItem
            // 
            this.uploadRicetteToolStripMenuItem.Enabled = false;
            this.uploadRicetteToolStripMenuItem.Name = "uploadRicetteToolStripMenuItem";
            this.uploadRicetteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.uploadRicetteToolStripMenuItem.Text = "Upload ricette";
            this.uploadRicetteToolStripMenuItem.Click += new System.EventHandler(this.uploadRicetteToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // forniToolStripMenuItem
            // 
            this.forniToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collegaFornoToolStripMenuItem,
            this.connessioniPrecedentiToolStripMenuItem,
            this.resettaDatabaseToolStripMenuItem});
            this.forniToolStripMenuItem.Name = "forniToolStripMenuItem";
            this.forniToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.forniToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.forniToolStripMenuItem.Text = "Fo&rni";
            // 
            // collegaFornoToolStripMenuItem
            // 
            this.collegaFornoToolStripMenuItem.Name = "collegaFornoToolStripMenuItem";
            this.collegaFornoToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.collegaFornoToolStripMenuItem.Text = "Parametri connessione...";
            this.collegaFornoToolStripMenuItem.Click += new System.EventHandler(this.collegaFornoToolStripMenuItem_Click);
            // 
            // connessioniPrecedentiToolStripMenuItem
            // 
            this.connessioniPrecedentiToolStripMenuItem.Name = "connessioniPrecedentiToolStripMenuItem";
            this.connessioniPrecedentiToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.connessioniPrecedentiToolStripMenuItem.Text = "Connessioni precedenti...";
            this.connessioniPrecedentiToolStripMenuItem.Click += new System.EventHandler(this.connessioniPrecedentiToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informazioniSuToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem1.Text = "?";
            // 
            // informazioniSuToolStripMenuItem
            // 
            this.informazioniSuToolStripMenuItem.Name = "informazioniSuToolStripMenuItem";
            this.informazioniSuToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.informazioniSuToolStripMenuItem.Text = "Informazioni su...";
            this.informazioniSuToolStripMenuItem.Click += new System.EventHandler(this.informazioniSuToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Red;
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.btnDisconnect);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(473, 46);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(336, 14);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(112, 23);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Location = new System.Drawing.Point(224, 14);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(106, 23);
            this.btnDisconnect.TabIndex = 2;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(76, 16);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(142, 20);
            this.txtAddress.TabIndex = 1;
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
            this.txtAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddress_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Indirizzo:";
            // 
            // resettaDatabaseToolStripMenuItem
            // 
            this.resettaDatabaseToolStripMenuItem.Name = "resettaDatabaseToolStripMenuItem";
            this.resettaDatabaseToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.resettaDatabaseToolStripMenuItem.Text = "Resetta database";
            this.resettaDatabaseToolStripMenuItem.Click += new System.EventHandler(this.resettaDatabaseToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(474, 762);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.FileMenuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.FileMenuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.FileMenuStrip1.ResumeLayout(false);
            this.FileMenuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip FileMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collegaFornoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem informazioniSuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connessioniPrecedentiToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem downloadRicetteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadRicetteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resettaDatabaseToolStripMenuItem;
    }
}

