﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class frmConnParameters : Form
    {
        private Boolean blnApply = false;
        public string m_user { get; set; }
        public string m_pass { get; set; }
        public string m_port { get; set; }

        public frmConnParameters()
        {
            InitializeComponent();
        }

        public new object ShowDialog()
        {
            txtUsername.Text = m_user;
            txtPwd.Text = m_pass;
            txtPort.Text = m_port;

            base.ShowDialog();

            if (blnApply)
            {
                return DialogResult.OK;
            }
            return DialogResult.Cancel;
        }

        #region Button's events

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            m_user = txtUsername.Text;
            m_pass = txtPwd.Text;
            m_port = txtPort.Text;
            blnApply = true;
        }

        #endregion
    }
}
