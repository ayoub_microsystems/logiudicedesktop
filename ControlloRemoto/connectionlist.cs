﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlloRemoto
{
    [Serializable]
    public class Connectiondata
    {
        public String address;
        public String username;
        public String password;
        public String port;
    }

    public class Connectionlist
    {
        private ArrayList mylist;

        public Connectionlist()
        {
            mylist = new ArrayList();
        }


        public int AddItem(Connectiondata obj)
        {
            return mylist.Add(obj);
        }

        public Connectiondata Item(int index)
        {
            return (Connectiondata)mylist[index];
        }

        public Boolean RemoveItem(Connectiondata obj)
        {
            int i;
            Boolean bRemoved = false;

            for (i = 0; i < mylist.Count; i++)
            {
                if (obj.Equals(mylist[i]))
                {
                    mylist.RemoveAt(i);
                    bRemoved = true;
                    break;
                }
            }
            return bRemoved;
        }

        public Boolean SearchItem(Connectiondata obj)
        {
            int i;
            Connectiondata cdata;
            Boolean bFound = false;

            for (i = 0; i < mylist.Count; i++)
            {
                cdata = (Connectiondata)mylist[i];
                if ((obj.address == cdata.address) &&
                    (obj.username == cdata.username) &&
                    (obj.password == cdata.password) &&
                    (obj.port == cdata.port))
                {
                    bFound = true;
                    break;
                }
            }
            return bFound;
        }

        public int ItemCount()
        {
            return mylist.Count;
        }
    }
}
