﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class frmNuovaRicetta : Form
    {
        DialogResult azione = DialogResult.Cancel;

        public frmNuovaRicetta()
        {
            InitializeComponent();

            txbNomeRic.MaxLength = 12;
        }

        public object ShowDialog(ref ClProg prog)
        {
            txbNomeRic.Text = "nuova ricetta";

            base.ShowDialog();

            if(azione == DialogResult.OK)
            {
                prog.InserisciNome(txbNomeRic.Text);
               
            }

            return azione;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            azione = DialogResult.OK;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            azione = DialogResult.Cancel;

            this.Close();
        }
    }
}
