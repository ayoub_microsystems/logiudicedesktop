﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class frmConnections : Form
    {
        private Boolean blnApply = false;
        public int iitemSelected;

        public frmConnections()
        {
            InitializeComponent();
        }
        
        public object ShowDialog(Connectionlist connlist)
        {
            TreeNode node;
            string sPwd;

            this.TopMost = true;


            treeView1.Nodes.Clear();

            for (int i = 0; i < connlist.ItemCount(); i++)
            {
                sPwd = "";
                for (int j = 0; j < connlist.Item(i).password.Length; j++)
                    sPwd += "*";
                node = treeView1.Nodes.Add(connlist.Item(i).address);

                node.Nodes.Add("Username: " + connlist.Item(i).username);
                node.Nodes.Add("Password: " + sPwd);
                node.Nodes.Add("Port: " + connlist.Item(i).port);
            }

            base.ShowDialog();

            if (blnApply)
            {
                return DialogResult.OK;
            }
            return DialogResult.Cancel;
        }

        #region Button's events

        private void btnApply_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;

            if (node != null)
            {
                if (node.Parent != null)
                    node = node.Parent;
                iitemSelected = node.Index;
                blnApply = true;
                this.Close();
            }
            else
                blnApply = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
