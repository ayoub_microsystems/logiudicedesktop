﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Win32;
using System.Globalization;
using System.ComponentModel;
using System.Threading;
//using Rotor_13.Properties;
//using Rotor_13.Lingue;
using System.Net;
using System.Media;
using FluentFTP;


namespace ControlloRemoto
{
    #region Interfacce
    public interface IparGen
    {
        void WriteFile();
        Object ReadFile();
     //   List<ClDescPar> DescPar();
    }
    #endregion

    #region Funzioni Generali
    
    public class Serialize
    {
        public static void SerializeObject(string filename, Object Cl)
        {
            XmlSerializer serializer = new XmlSerializer(Cl.GetType());

            // Writing the document requires a TextWriter.
            TextWriter writer = new StreamWriter(filename);

            // Serialize the object, and close the TextWriter.
            try
            {
                serializer.Serialize(writer, Cl);
            }
            catch
            {
            }
            writer.Close();
        }
        public void SerializeObject(string filename)
        {
            SerializeObject(filename, this);
        }
        public static Object DeserializeObject(string filename, Object Cl)
        {
            // Create an instance of the XmlSerializer specifying type and namespace.
            XmlSerializer serializer = new XmlSerializer(Cl.GetType());

            // A FileStream is needed to read the XML document.

            if (File.Exists(filename))
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                XmlReader reader = XmlReader.Create(fs);

                // Use the Deserialize method to restore the object's state.
                try
                {
                    Cl = serializer.Deserialize(reader);
                }
                catch
                {

                }

                fs.Close();
            }

            return Cl;
        }
        public Object DeserializeObject(string filename)
        {
            return DeserializeObject(filename, this);
        }

    }

    public class FunzioniGenerali : Serialize, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
   //     private static string _workingDirectory = null;

        public static Bitmap LoadBitmapResource(string strName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            AssemblyName NomeProgetto = assembly.GetName();
            string strRes = NomeProgetto.Name.ToString() + ".Immagini." + strName;
            Stream stream = assembly.GetManifestResourceStream(strRes);
            Bitmap bmp = null;
            try
            {
                bmp = new Bitmap(stream);
            }
            catch { }

            stream.Close();

            return bmp;
        }
        public T MinMax<T>(T val, T min, T max) where T : IComparable<T>
        {
            if (val is Byte)
            {
                Byte ValB = Convert.ToByte(val);
                if (ValB == Byte.MaxValue)                  // se valore è max lo pongo al massimo passato. Xe sono passato da 0 a 255
                    return max;
            }
            if (val.CompareTo(min) < 0)
                return max;
            else if (val.CompareTo(max) > 0)
                return min;
            else
                return val;
        }
        public string CreatePathFile(string strFile)
        {
           //string DirectoryExe = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            //string DirectoryExeFile = Path.Combine(DirectoryExe, strFile);

            //return DirectoryExeFile;

            return @"C:\DatiFornoRotor\" + strFile;
        }
        public object CopiaTo(object Dest)
        {
            PropertyInfo[] CampiDest = Dest.GetType().GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] CampiSorg = this.GetType().GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);

            for (int i = 0; i < CampiDest.Length; i++)
            {
                object Val = CampiDest[i].GetValue(this, null);
                if (CampiSorg[i].CanWrite)
                    CampiSorg[i].SetValue(Dest, Val, null);
            }
            return Dest;


            /*
            FieldInfo[] CampiDest = Dest.GetType().GetFields(BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo[] CampiSorg = this.GetType().GetFields(BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Instance);

            for (int i = 0; i < CampiDest.Length; i++)
            {
                object Val = CampiDest[i].GetValue(this);
                CampiSorg[i].SetValue(Dest, Val);
            }
            return Dest;
            */
        }
        public bool CompareTo(object Dest)
        {
            PropertyInfo[] CampiDest = Dest.GetType().GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] CampiSorg = this.GetType().GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);

            //bool aa = CampiDest.SequenceEqual(CampiSorg, );

            for (int i = 0; i < CampiDest.Length; i++)
            {
                object Obj1 = CampiDest[i].GetValue(Dest, null);
                object Obj2 = CampiSorg[i].GetValue(this, null);

                if (Obj1.Equals(Obj2) == false)
                    return false;
            }
            return true;
        }
        public static void Populate<T>(T[] Arr, T value)
        {
            for (int i = 0; i < Arr.Length; i++)
                Arr[i] = value;
        }
        public static short ConvCtoF(short val, short Offset)
        {
            return (short)((val * 9) / 5 + Offset);
        }
        public static short ConvFtoC(short val, short Offset)
        {
            short resto, memVal;
                memVal = val;
                val = (short)((val - Offset) * 5);
                resto = (short)(val % 9);
                val = (short)(val / 9);


                if (resto != 0)
                    if (memVal < 0)
                        return (short)(val - 1);
                    else
                        return (short)(val + 1);

                return val;
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public static void DrawLabel(Label label, Graphics grx, Control pb)
        {
            Rectangle Rect = new Rectangle(label.Left - pb.Left, label.Top - pb.Top, label.Width, label.Height);

            if (label.TextAlign == ContentAlignment.TopLeft)
            {
                grx.DrawString(label.Text, label.Font, new SolidBrush(label.ForeColor), Rect);

            }
            else if (label.TextAlign == ContentAlignment.TopCenter)
            {
                SizeF size = grx.MeasureString(label.Text, label.Font);
                int left = label.Width / 2 - (int)size.Width / 2 + label.Left;
                Rectangle rect = new Rectangle(left - pb.Left, label.Top - pb.Top, (int)size.Width, (int)label.Height);
                grx.DrawString(label.Text, label.Font, new SolidBrush(label.ForeColor), rect);
            }
            else if (label.TextAlign == ContentAlignment.TopRight)
            {
                SizeF size = grx.MeasureString(label.Text, label.Font);
                int left = label.Width - (int)size.Width + Rect.Left;
                Rectangle rect = new Rectangle(left, Rect.Top, (int)size.Width, (int)size.Height);
                grx.DrawString(label.Text, label.Font, new SolidBrush(label.ForeColor), rect);
            }
        }
        public static ushort ToUShort(byte byte1, byte byte2)
        {
            return (ushort)((((int)byte1) << 8) | (int)byte2);
        }
        public static Bitmap loadBitmap(string absFileName)
        {
            try
            {
                if (!Path.IsPathRooted(absFileName))
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }


            Bitmap bmp = null;
            try
            {
                using (FileStream fl = new FileStream(absFileName, FileMode.Open))
                {
                    using (BinaryReader br = new BinaryReader(fl))
                    {
                        byte[] buffer = new byte[fl.Length];
                        br.Read(buffer, 0, (int)fl.Length);
                        using (MemoryStream mr = new MemoryStream(buffer))
                        {
                            bmp = new Bitmap(mr);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                return null;
            }

            return bmp;
        }

   /*     public static Bitmap caricaImmagine(string nomeFileImmagine)
        {
            if (nomeFileImmagine == null)
            {
                return null;
            }

            nomeFileImmagine = getWorkingDirectory() + ".Immagini." + nomeFileImmagine;

            return loadBitmap(nomeFileImmagine);
        }*/



       
        /// <summary>
        /// Restituisce il path assoluto di lavoro del programma in esecuzione
        /// </summary>
        /// <returns>Il path assoluto di lavoro del programma in esecuzione</returns>
     /*   public static string getWorkingDirectory()
        {
            if (_workingDirectory == null)
            {
                string strAssmPath = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
                _workingDirectory = strAssmPath.Substring(0, strAssmPath.LastIndexOf(@"\") + 1);
            }
            return _workingDirectory;
        }*/


        #region INotifyPropertyChanged Membri di

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }

        #endregion
    }
    #endregion

    #region Funzioni specifiche
    public class FunzSpec
    {
        public static string CaricaCultura(ClParStr.EnLingua Language)
        {
            switch (Language)
            {
                case ClParStr.EnLingua.IT:
                    return "it";

                case ClParStr.EnLingua.EN:
                    return "en";


                case ClParStr.EnLingua.FR:
                    return "fr";

                case ClParStr.EnLingua.ES:
                    return "es";

                case ClParStr.EnLingua.RU:
                    return "ru";
                case ClParStr.EnLingua.DE:
                    return "de";
              
                default:
                    return CultureInfo.CurrentCulture.Name;
            }
        }


    }

    #endregion


    #region DatiInterniForno

    //public class ClDatiInterniForno : FunzioniGenerali
    //{
    //    #region NomeFile
    //    private readonly string FILE_DATI_INT_FOR = "DatiIntForno.xml";
    //    #endregion

    //    #region Campi

    //    public Byte Nstrumento { get; set; }		//numero strumento
    //    public Byte Baud { get; set; }			//baudRate	
    //    public Byte NumProg { get; set; }	   // programma corrente
    //    public bool FornoAcceso { get; set; }	   // programma corrente
    //    #endregion

    //    #region Metodi

    //    public void WriteFile()
    //    {
    //        this.SerializeObject(CreatePathFile(FILE_DATI_INT_FOR));
    //    }

    //    public ClDatiInterniForno ReadFile()
    //    {
    //        return (ClDatiInterniForno)this.DeserializeObject(CreatePathFile(FILE_DATI_INT_FOR));
    //    }


    //    #endregion
    //}

    public class ClParStr : FunzioniGenerali, IparGen
    {
        readonly string FILE_PAR_STRUMENTO = "ParImpostazione.xml";

        #region Enum
        public enum EnLingua { IT, EN, ES, FR, RU, DE, MAX };
        public enum EnScala { SCALA_GC, SCALA_GF, MAX };
        //public enum EnSuoniTouch { OFF, SOUND1, SOUND2, MAX }

        #endregion

        #region Costruttore
        public ClParStr()
        {
            //Luminosita = 250;
            //TimeoutLCD = 60;
            Lingua = EnLingua.IT;
            Numcamere = 1; //numero camere
            Scala = EnScala.SCALA_GC;
            SP_tass = 608;
            tmax_vap = 10;

            //Prosetsep = Global.SiNo.NO;
            //SuoniTouch = EnSuoniTouch.OFF;
        }
        #endregion

        #region Campi
        //private int mLuminosita;			// luminosità
        //private int mTimeoutLCD;			// timeout lampada
        private EnLingua mLingua;           // lingua selezionata
        private short mNumcamere;	        // numero camere
        private EnScala mScala;             // 1 scala °C/°F (2)
        private short msp_tass;
        private short mtmax_vap;

        //private Global.SiNo mProsetsep;	    // prog.sett.separata
        //private EnSuoniTouch mSuoniTouch;   // suoni al touch

        [XmlIgnoreAttribute]
        //private int mSetLuminosita; 
        //private bool EnConteggio;
        //private int MemSec;
        //private int CounterLcd;
        //private bool EnDisplay;

        #endregion

        #region Proprieta
        //public int Luminosita
        //{
        //    get { return mLuminosita; }
        //    set { mLuminosita = MinMax<int>(value, 50, 250); OnPropertyChanged("Luminosita"); }
        //}
        //public int TimeoutLCD
        //{
        //    get { return mTimeoutLCD; }
        //    set { mTimeoutLCD = MinMax<int>(value, 0, 240); OnPropertyChanged("TimeoutLCD"); }
        //}
        public EnLingua Lingua
        {
            get { return mLingua; }
            set { mLingua = (EnLingua)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnLingua.IT), Convert.ToInt32(EnLingua.MAX) - 1); OnPropertyChanged("Lingua"); }
        }
        public short Numcamere
        {
            get { return mNumcamere; }
            set { mNumcamere = MinMax<short>(value, 1, 32); OnPropertyChanged("Numcamere"); }
        }
        public EnScala Scala
        {
            get { return mScala; }
            set { mScala = (EnScala)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnScala.SCALA_GC), Convert.ToInt32(EnScala.MAX) - 1); OnPropertyChanged("Scala"); }
        }
        public Int16 SP_tass
        {
            get { return msp_tass; }
            set { msp_tass = value; }//MinMax<Int16>(ConvCtoF(value, 32), 32, (short)((Global.ParSotteranei.sp_tass * 9) / 5 + 32)); OnPropertyChanged("set_temp"); }      // mettere max da par sotterraneo
        }
        public Int16 tmax_vap
        {
            get { return mtmax_vap; }
            set { mtmax_vap = value; }
        }

        //public Global.SiNo Prosetsep
        //{
        //    get { return mProsetsep; }
        //    set { mProsetsep = (Global.SiNo)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.SiNo.NO), Convert.ToInt32(Global.SiNo.MAX) - 1); OnPropertyChanged("Prosetsep"); }
        //}

        //public EnSuoniTouch SuoniTouch
        //{
        //    get { return mSuoniTouch; }
        //    set { mSuoniTouch = (EnSuoniTouch)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnSuoniTouch.OFF), Convert.ToInt32(EnSuoniTouch.MAX) - 1); OnPropertyChanged("SuoniTouch"); }
        //}

        //[XmlIgnoreAttribute]
        /*
        public int SetLuminosita
        {
            get { return Luminosita; }
            set {
                Luminosita = value;
                mSetLuminosita = Luminosita;
                LuminositàLampada.SetBacklight(Convert.ToByte(Luminosita));        // inserisco la luminosità effettiva
                OnPropertyChanged("SetLuminosita");
            }
        }
         */
        #endregion

        #region Metodi
        //public void LampadaLCD(bool isAllarm)
        //{
        //    if (DateTime.Now.Second != MemSec)          // ogni secondo
        //    {
        //        MemSec = DateTime.Now.Second;

        //        Byte ValLum = Convert.ToByte(Luminosita);

        //        if (Luminosita == 0)                    // se timeout a zero
        //            EnConteggio = false;                // disabilito spegnimento

        //        if (EnDisplay)
        //        {
        //            if (EnConteggio && isAllarm == false && TimeoutLCD > 0)      // se conteggio abilitato e set > 0
        //            {
        //                if (CounterLcd > 0)
        //                    CounterLcd--;
        //                else
        //                    ValLum = 40;
        //            }
        //        }
        //        else
        //            ValLum = 40;

        //        LuminositàLampada.SetBacklight(ValLum);
        //    }
        //}
        //public void EnDisCounterLCD(bool set)
        //{
        //    EnConteggio = set;
        //}
        //public void SetCounterLCD()
        //{
        //    CounterLcd = TimeoutLCD;
        //}
        //public void SetDisplayLCD(bool set)
        //{
        //    EnDisplay = set;

        //    if (EnDisplay)
        //        LuminositàLampada.SetBacklight(Convert.ToByte(Luminosita)); // immediatamente
        //    else
        //        LuminositàLampada.SetBacklight(40);             // immediatamente
        //}
        public void WriteFile()
        {
            //Resource.Culture = new CultureInfo(FunzSpec.CaricaCultura(Lingua));  // se impostata salvo anche la lingua
            SerializeObject(CreatePathFile(FILE_PAR_STRUMENTO));
        }
        public Object ReadFile()
        {
            return DeserializeObject(CreatePathFile(FILE_PAR_STRUMENTO));
        }


        #endregion
    }
    #endregion


    #region RAMBK
    //public class ClRAMBK : FunzioniGenerali
    //{
    //    #region Costruttore
    //    public ClRAMBK()
    //    {
    //        hRAMBK = (IntPtr)CoreDLL.INVALID_HANDLE_VALUE;
    //        hRAMBK = CoreDLL.CreateFile("RMB1:");
    //        if (hRAMBK == (IntPtr)CoreDLL.INVALID_HANDLE_VALUE)
    //        {
    //            int error = Marshal.GetLastWin32Error();
    //            string wotchdogError = String.Format("CreateFile Failed: {0}", error);
    //        }


    //    }
    //    #endregion

    //    #region enum
    //    [Flags]
    //    public enum Enflaggenerici : byte
    //    {
    //        Enflag_bruc_ok = 1,             // on/off 
    //        Enflag_vent_ok = 2,             // on/off 
    //        FLAGBK_TMPPROG = 8,
    //        FLAG_ECO_ON = 16,
    //        velocita_bassa=32,

    //    };
    //    [Flags]
    //    public enum Enflaggenerici2 : byte
    //    {
    //      //  FLAG_FORNO_ON = 1,		   
    //        FLAG_START_COTTURA = 2,		      
    //        FLAG_VAPORE_ON = 4,          
    //        FLAG_LUCE = 8,           
    //        FLAG_ASPIRATORE_ON = 16,          
    //        FLAG_SERRANDA_ON = 32,         
    //        FLAG_CARRELLO_ON = 64,         

    //    };
    //    public enum EnBackupOUT_RELE : byte
    //    {
    //        OUT_R1_LUCE = 1,
    //        OUT_R2_ASPIRATORE = 2,
    //        OUT_R3_CARRELLO = 4,
    //        OUT_R4_BRUCIATORE = 8,
    //        OUT_R5_VAPORE = 16,
    //        OUT_R6_VENTILAZIONE = 32,
    //        OUT_R7_SERRANDA = 64,
    //        OUT_R8_BRUC_PELLET = 128,
    //    } ;
    //    #endregion


    //    #region Campi
    //    public Byte okclock { get; set; }				// 
    //    private Enflaggenerici flaggenerici;
    //    private Enflaggenerici2 flaggenerici2;
    //    private EnBackupOUT_RELE BackupOUT_RELE;
    //    private Byte mNumFase { get; set; }					// 
    //    public ClGestFornoLo.EnStati StatoMacchina;
    //    public Int16 mTempSet { get; set; }				//
    //    public SByte TempVap { get; set; }
    //    public Int16 TempCott { get; set; }			// 
    //    public Int16 TempBrucStart { get; set; }			// 
    //    public Int16 TempSbuffi { get; set; }			// 
    //    public Int16 TempVentEnd { get; set; }			// 
    //    public Int16 TempVentStart { get; set; }			// 
    //    public Int16 TempSerr { get; set; }			// 
    //    public SByte Nsbuffi { get; set; }
    //    public Int16 RitVapore { get; set; }
    //    public Int16 TempoTOT { get; set; }
    //    public bool ERROR_CLOCK { get; set; }           // errore orologio
    //    private IntPtr hRAMBK;
    //    private bool ScriviRam;

    //    #endregion



    //    #region Proprieta

    //    public Int16 TempSet
    //    {
    //        get { return ConvFtoC(mTempSet, 32); }
    //        set { mTempSet = MinMax<Int16>(ConvCtoF(value, 32), 32, (short)((Global.ParSotteranei.sp_tass * 9) / 5 + 32)); }      
    //    }

    //    public Byte NumFase
    //    {
    //        get { return mNumFase; }
    //        set { mNumFase = MinMax<Byte>(value, 0, 7); OnPropertyChanged("NumFase"); }
    //    }
    //    //**    Enflaggenerici   **
    //    public bool flag_bruc_ok
    //    {
    //        get { return (flaggenerici & Enflaggenerici.Enflag_bruc_ok) > 0; }
    //        set { if (value) flaggenerici |= Enflaggenerici.Enflag_bruc_ok; else flaggenerici &= ~Enflaggenerici.Enflag_bruc_ok; }
    //    }
    //    public bool flag_vent_ok
    //    {
    //        get { return (flaggenerici & Enflaggenerici.Enflag_vent_ok) > 0; }
    //        set { if (value) flaggenerici |= Enflaggenerici.Enflag_vent_ok; else flaggenerici &= ~Enflaggenerici.Enflag_vent_ok; }
    //    }

    //    public bool FLAGBK_TMPPROG
    //    {
    //        get { return (flaggenerici & Enflaggenerici.FLAGBK_TMPPROG) > 0; }
    //        set { if (value) flaggenerici |= Enflaggenerici.FLAGBK_TMPPROG; else flaggenerici &= ~Enflaggenerici.FLAGBK_TMPPROG; }
    //    }
    //    public bool FLAG_ECO_ON
    //    {
    //        get { return (flaggenerici & Enflaggenerici.FLAG_ECO_ON) > 0; }
    //        set { if (value) flaggenerici |= Enflaggenerici.FLAG_ECO_ON; else flaggenerici &= ~Enflaggenerici.FLAG_ECO_ON; }
    //    }
    //    public bool velocita_bassa
    //    {
    //        get { return (flaggenerici & Enflaggenerici.velocita_bassa) > 0; }
    //        set { if (value) flaggenerici |= Enflaggenerici.velocita_bassa; else flaggenerici &= ~Enflaggenerici.velocita_bassa; }
    //    }

    //    //**    Enflaggenerici2   **
    //  /*  public bool FLAG_FORNO_ON
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_FORNO_ON) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_FORNO_ON; else flaggenerici2 &= ~Enflaggenerici2.FLAG_FORNO_ON; }
    //    }*/
    //    public bool FLAG_START_COTTURA
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_START_COTTURA) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_START_COTTURA; else flaggenerici2 &= ~Enflaggenerici2.FLAG_START_COTTURA; }
    //    }

    //    public bool FLAG_VAPORE_ON
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_VAPORE_ON) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_VAPORE_ON; else flaggenerici2 &= ~Enflaggenerici2.FLAG_VAPORE_ON; }
    //    }

    //    public bool FLAG_LUCE
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_LUCE) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_LUCE; else flaggenerici2 &= ~Enflaggenerici2.FLAG_LUCE; }
    //    }
    //    public bool FLAG_ASPIRATORE_ON
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_ASPIRATORE_ON) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_ASPIRATORE_ON; else flaggenerici2 &= ~Enflaggenerici2.FLAG_ASPIRATORE_ON; }
    //    }
    //    public bool FLAG_SERRANDA_ON
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_SERRANDA_ON) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_SERRANDA_ON; else flaggenerici2 &= ~Enflaggenerici2.FLAG_SERRANDA_ON; }
    //    }
    //    public bool FLAG_CARRELLO_ON
    //    {
    //        get { return (flaggenerici2 & Enflaggenerici2.FLAG_CARRELLO_ON) > 0; }
    //        set { if (value) flaggenerici2 |= Enflaggenerici2.FLAG_CARRELLO_ON; else flaggenerici2 &= ~Enflaggenerici2.FLAG_CARRELLO_ON; }
    //    }

    //    //**    EnBackupOUT_RELE   **
    //    public bool OUT_R1_LUCE
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R1_LUCE) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R1_LUCE; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R1_LUCE; }
    //    }
    //    public bool OUT_R2_ASPIRATORE
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R2_ASPIRATORE) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R2_ASPIRATORE; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R2_ASPIRATORE; }
    //    }

    //    public bool OUT_R3_CARRELLO
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R3_CARRELLO) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R3_CARRELLO; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R3_CARRELLO; }
    //    }

    //    public bool OUT_R4_BRUCIATORE
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R4_BRUCIATORE) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R4_BRUCIATORE; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R4_BRUCIATORE; }
    //    }

    //    public bool OUT_R5_VAPORE
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R5_VAPORE) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R5_VAPORE; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R5_VAPORE; }
    //    }


    //    public bool OUT_R6_VENTILAZIONE
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R6_VENTILAZIONE) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R6_VENTILAZIONE; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R6_VENTILAZIONE; }
    //    }


    //    public bool OUT_R7_SERRANDA
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R7_SERRANDA) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R7_SERRANDA; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R7_SERRANDA; }
    //    }

    //    public bool OUT_R8_BRUC_PELLET
    //    {
    //        get { return (BackupOUT_RELE & EnBackupOUT_RELE.OUT_R8_BRUC_PELLET) > 0; }
    //        set { if (value) BackupOUT_RELE |= EnBackupOUT_RELE.OUT_R8_BRUC_PELLET; else BackupOUT_RELE &= ~EnBackupOUT_RELE.OUT_R8_BRUC_PELLET; }
    //    }


    //    #endregion

    //    #region Metodi
    //    public void ScriviRAMBK()
    //    {
    //        ushort StartAddress = 0;                    // inizio ram back up
    //        int written = 0;

    //        if ((DateTime.Now.Second % 30) == 0)        // ogni 30 secondi
    //        {
    //            if (ScriviRam == false)                 // per scrivere una volta sola
    //            {
    //                ScriviRam = true;
    //                List<Byte> buf = new List<byte>();
    //                buf.Add(okclock);
    //                buf.Add(NumFase);
    //                buf.Add(Convert.ToByte(flaggenerici));
    //                buf.Add(Convert.ToByte(flaggenerici2));
    //                buf.Add(Convert.ToByte(BackupOUT_RELE));
    //                buf.Add(Convert.ToByte(StatoMacchina));
    //                buf.AddRange(BitConverter.GetBytes(TempSet));
    //                buf.Add((byte)TempVap);
    //                buf.AddRange(BitConverter.GetBytes(TempCott));
    //                buf.AddRange(BitConverter.GetBytes(TempBrucStart));
    //                buf.AddRange(BitConverter.GetBytes(TempSbuffi));
    //                buf.AddRange(BitConverter.GetBytes(TempVentEnd));
    //                buf.AddRange(BitConverter.GetBytes(TempVentStart));
    //                buf.AddRange(BitConverter.GetBytes(TempSerr));
    //                buf.Add((byte)Nsbuffi);
    //                buf.AddRange(BitConverter.GetBytes(RitVapore));
    //                buf.AddRange(BitConverter.GetBytes(TempoTOT));

    //                CoreDLL.CEWriteFile(hRAMBK, buf.ToArray(), (UInt32)((StartAddress << 16) + buf.Count), ref written, IntPtr.Zero);
    //            }
    //        }
    //        else
    //            ScriviRam = false;
    //    }
    //    public void LeggiRAMBK()
    //    {
    //        Byte[] buffer = new byte[56];
    //        Int16 StartAddress = 0;                // 0 = inizio ram backup, 
    //        int read = 0;
    //        int offset = 0;
    //        CoreDLL.CEReadFile(hRAMBK, buffer, (UInt32)((StartAddress << 16) + buffer.Length), ref read, IntPtr.Zero);

    //        okclock = buffer[offset++];

    //        if (okclock != 0x55)                        // se Ram sprogrammata
    //        {
    //            Array.Clear(buffer, 0, buffer.Length);  // resetto tutta la Ram
    //            okclock = 0x55;
    //            ERROR_CLOCK = true;                     // setto errore orologio
    //        }

    //        NumFase = buffer[offset++];
    //        flaggenerici = (Enflaggenerici)buffer[offset++];
    //        flaggenerici2 = (Enflaggenerici2)buffer[offset++];
    //        BackupOUT_RELE = (EnBackupOUT_RELE)buffer[offset++];

    //        StatoMacchina = (ClGestFornoLo.EnStati)buffer[offset++];
    //        TempSet = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempSet);
    //        TempVap = (SByte)buffer[offset++];
    //        TempCott = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempCott);
    //        TempBrucStart = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempBrucStart);
    //        TempSbuffi = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempSbuffi);
    //        TempVentEnd = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempVentEnd);
    //        TempVentStart = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempVentStart);
    //        TempSerr = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempSerr);
    //        Nsbuffi = (SByte)buffer[offset++];
    //        RitVapore = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(RitVapore);
    //        TempoTOT = BitConverter.ToInt16(buffer, offset); offset += Marshal.SizeOf(TempoTOT);
    //    }

    //    public Int16 TempCottOre()
    //    {
    //        return (Int16)(TempCott / 60);
    //    }
    //    public Int16 TempCottMinuti()
    //    {
    //        return (Int16)(TempCott % 60);
    //    }

    //    public Int16 TempTOTOre()
    //    {
    //        return (Int16)(TempoTOT / 60);
    //    }
    //    public Int16 TempoTOTMinuti()
    //    {
    //        return (Int16)(TempoTOT % 60);
    //    }


    //    #endregion


    //}
    #endregion


    #region Parametri Sotterranei
    //public class ClParSott : FunzioniGenerali
    //{
    //    readonly string FILE_PAR_SOTT = "ParSotteranei.xml";

    //    #region Costruttore
    //    public ClParSott()
    //    {

    //        ResetParSott();

    //    }
    //    #endregion

    //    #region Enum
    //    public enum EnModforno { ELETTRICO, COMBUSTIONE, PELLET, MAX };
    //    public enum EnScala { SCALA_GC, SCALA_GF, MAX };
    //    public enum EnStadio { MONO, BISTADIO, MAX };
    //    public enum EnCarrello { ISTANTANEO, FINE_CICLO, MAX };
    //    public enum EnTipoBuzzer { PULSANTE, CONTINUO, MAX };
    //    #endregion

    //    #region Campi
    //    private EnModforno mModforno;	    //  modello forno 
    //    private EnScala mScala;         // 1 scala °C/°F (2)
    //    private EnStadio mN_stadi;
    //    public short mdiff_reg;
    //    private short msp_tass;			  // set camera massimo impostabile
    //    private short mtmax_vap;			  // tempo massimo vapore
    //    private Global.OnOff mdbl_speed;		  // parametro doppia velocità
    //    private byte mrit_cpor;           // ritardo partenza ventilatore dopo chiusura porta
    //    private short mtempo_fc_carrello;    // tempo limite finecorsa carrello
    //    private EnCarrello mmod_ferma_carrello; // modalita arresto carrello
    //    private Global.OnOff mfunzione_eco;
    //    private Global.OnOff mVal_automatica;
    //    private short mminuti_luce;	  // gestione tempo luce accesa	
    //    private short mOffset_Temp;   	// offset temperatura
    //    private Global.OnOff mLievitazione;// cella lievigtazione
    //    private Byte mPassPar1;
    //    private Byte mPassPar2;
    //    private Byte mPassPar3;
    //    private Byte mPassPar4;
    //    private EnTipoBuzzer mbuz_type;
    //    private Global.OnOff mAbilitaCanFoto;
    //    #endregion

    //    #region Proprietà
    //    public EnModforno Modforno
    //    {
    //        get { return mModforno; }
    //        set { mModforno = (EnModforno)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnModforno.ELETTRICO), Convert.ToInt32(EnModforno.MAX) - 1); OnPropertyChanged("Modforno"); }
    //    }
    //    public EnScala Scala
    //    {
    //        get { return mScala; }
    //        set { mScala = (EnScala)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnScala.SCALA_GC), Convert.ToInt32(EnScala.MAX) - 1); OnPropertyChanged("Scala"); }
    //    }



    //    public EnStadio N_stadi
    //    {
    //        get { return mN_stadi; }
    //        set { mN_stadi = (EnStadio)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnStadio.MONO), Convert.ToInt32(EnStadio.MAX) - 1); OnPropertyChanged("N_stadi"); }
    //    }
    //    public short diff_reg
    //    {
    //        get { return ConvFtoC(mdiff_reg, 0); }
    //        set { mdiff_reg = MinMax<short>(ConvCtoF(value, 0), 1, 18); OnPropertyChanged("diff_reg"); }        //diff.risc.(1°F=1°C ÷ 18°F=10°C)
    //    }
    //    public short sp_tass
    //    {
    //      //  get { return ConvFtoC(msp_tass, 32); }
    //        get { return msp_tass; }
    //      //  set { msp_tass = MinMax<short>(ConvCtoF(value, 32), 32, 752); OnPropertyChanged("sp_tass"); }
    //         set { msp_tass = MinMax<short>(value, 0, 320); OnPropertyChanged("sp_tass"); }
    //    }
    //    public short tmax_vap
    //    {
    //        get { return mtmax_vap; }
    //        set { mtmax_vap = MinMax<short>(value, 0, 99); OnPropertyChanged("tmax_vap"); }
    //    }
    //    public Global.OnOff dbl_speed
    //    {
    //        get { return mdbl_speed; }
    //        set { mdbl_speed = (Global.OnOff)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.OnOff.OFF), Convert.ToInt32(Global.OnOff.MAX) - 1); OnPropertyChanged("dbl_speed"); }

    //    }
    //    public byte rit_cpor
    //    {
    //        get { return mrit_cpor; }
    //        set { mrit_cpor = MinMax<byte>(value, 0, 99); OnPropertyChanged("rit_cpor"); }
    //    }
    //    public short tempo_fc_carrello
    //    {
    //        get { return mtempo_fc_carrello; }
    //        set { mtempo_fc_carrello = MinMax<short>(value, 0, 99); OnPropertyChanged("tempo_fc_carrello"); }
    //    }
    //    public EnCarrello mod_ferma_carrello
    //    {
    //        get { return mmod_ferma_carrello; }
    //        set { mmod_ferma_carrello = (EnCarrello)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnCarrello.ISTANTANEO), Convert.ToInt32(EnModforno.MAX) - 1); OnPropertyChanged("mod_ferma_carrello"); }
    //    }
    //    public Global.OnOff funzione_eco
    //    {
    //        get { return mfunzione_eco; }
    //        set { mfunzione_eco = (Global.OnOff)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.OnOff.OFF), Convert.ToInt32(Global.OnOff.MAX) - 1); OnPropertyChanged("funzione_eco"); }
    //    }

    //    public Global.OnOff AbilitaCanFoto
    //    {
    //        get { return mAbilitaCanFoto; }
    //        set { mAbilitaCanFoto = (Global.OnOff)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.OnOff.OFF), Convert.ToInt32(Global.OnOff.MAX) - 1); OnPropertyChanged("AbilitaCanFoto"); }
    //    }

    //    public Global.OnOff Val_automatica
    //    {
    //        get { return mVal_automatica; }
    //        set { mVal_automatica = (Global.OnOff)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.OnOff.OFF), Convert.ToInt32(Global.OnOff.MAX) - 1); OnPropertyChanged("Val_automatica"); }
    //    }
    //    public short minuti_luce
    //    {
    //        get { return mminuti_luce; }
    //        set { mminuti_luce = MinMax<short>(value, 0, 999); OnPropertyChanged("minuti_luce"); }
    //    }
    //    public short Offset_Temp
    //    {

    //        get { return ConvFtoC(mOffset_Temp, 0);}
    //        set { mOffset_Temp = MinMax<short>(ConvCtoF(value, 0), -180, 180); OnPropertyChanged("Offset_Temp"); }
    //    }
    //    public Global.OnOff Lievitazione
    //    {
    //        get { return mLievitazione; }
    //        set { mLievitazione = (Global.OnOff)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(Global.OnOff.OFF), Convert.ToInt32(Global.OnOff.MAX) - 1); OnPropertyChanged("Lievitazione"); }
    //    }

    //    public Byte PassPar1
    //    {
    //        get { return mPassPar1; }
    //        set { mPassPar1 = MinMax<Byte>(value, 0, 9); OnPropertyChanged("PassPar1"); }
    //    }
    //    public Byte PassPar2
    //    {
    //        get { return mPassPar2; }
    //        set { mPassPar2 = MinMax<Byte>(value, 0, 9); OnPropertyChanged("PassPar1"); }
    //    }
    //    public Byte PassPar3
    //    {
    //        get { return mPassPar3; }
    //        set { mPassPar3 = MinMax<Byte>(value, 0, 9); OnPropertyChanged("PassPar1"); }
    //    }
    //    public Byte PassPar4
    //    {
    //        get { return mPassPar4; }
    //        set { mPassPar4 = MinMax<Byte>(value, 0, 9); OnPropertyChanged("PassPar1"); }
    //    }

    //    public EnTipoBuzzer buz_type
    //    {
    //        get { return mbuz_type; }
    //        set { mbuz_type = (EnTipoBuzzer)MinMax<int>(Convert.ToInt32(value), Convert.ToInt32(EnTipoBuzzer.PULSANTE), Convert.ToInt32(EnTipoBuzzer.MAX) - 1); OnPropertyChanged("buz_type"); }
    //    }



    //    #endregion

    //    #region Metodi

    //    public void ResetParSott()
    //    {

    //        mModforno = EnModforno.COMBUSTIONE  ;	    //  modello forno 
    //        mScala = EnScala.SCALA_GC;         // 1 scala °C/°F (2)
    //        mN_stadi = 0;
    //        mdiff_reg = 2;
    //        msp_tass = 608;			  // set camera massimo impostabile
    //        mtmax_vap = 10;			  // tempo massimo vapore
    //        mdbl_speed = Global.OnOff.OFF;		  // parametro doppia velocità
    //        mrit_cpor = 5;           // ritardo partenza ventilatore dopo chiusura porta
    //        mtempo_fc_carrello = 0;    // tempo limite finecorsa carrello
    //        mmod_ferma_carrello = EnCarrello.ISTANTANEO; // modalita arresto carrello
    //        mfunzione_eco = Global.OnOff.OFF;
    //        mVal_automatica = Global.OnOff.OFF;
    //        mminuti_luce = 0;	  // gestione tempo luce accesa	
    //        mOffset_Temp = 0;   	// offset temperatura
    //        mLievitazione = Global.OnOff.OFF;// cella lievigtazione
    //        mPassPar1=0;
    //        mPassPar2=0;
    //        mPassPar3=0;
    //        mPassPar4=0;
    //        mbuz_type = EnTipoBuzzer.PULSANTE;
    //        mAbilitaCanFoto = 0;

    //    }
    //    public void WriteFile()
    //    {
    //        this.SerializeObject(CreatePathFile(FILE_PAR_SOTT));
    //    }

    //    public ClParSott ReadFile()
    //    {
    //        return (ClParSott)this.DeserializeObject(CreatePathFile(FILE_PAR_SOTT));
    //    }
    //    #endregion


    //}

    #endregion


    #region Ricette
    public class ClProg : FunzioniGenerali
    {
   
        static readonly int NUM_FASI = (8 + 1);
        static readonly int SZ_NOME = (12 + 1);
        static readonly int SZ_PARPROG = (7 + 1);
        public static readonly int SZ_PRG = ((NUM_FASI - 1) * 20 + SZ_NOME * 2 + SZ_PARPROG + 1);

        #region Campi
        private Char[] mNome = new Char[SZ_NOME];
        private Char[] mNomeFig = new Char[SZ_NOME];
      
        
        private ClfaseNew[] mfase = new ClfaseNew[NUM_FASI];
        private ClParprog mParprog = new ClParprog();
        #endregion

        #region Costruttore
        public ClProg()
        {

            for (int i = 0; i < fase.Length; i++)
            {
                fase[i] = new ClfaseNew();
                if (i > 0)
                    fase[i].tmp_vapore = 0;
            }
        }
        #endregion

        #region Proprieta
        public Char[] Nome
        {
            get { return mNome; }
            set { mNome = value; OnPropertyChanged("Nome"); }
        }
        public Char[] NomeFig
        {

            get { return mNomeFig; }     // copio il nome  ; }
            set { mNomeFig = value; OnPropertyChanged("NomeFig"); }
        }
        public ClfaseNew[] fase
        {
            get { return mfase; }
            set { mfase = value; OnPropertyChanged("fase"); }
        }
        public ClParprog Parprog
        {
            get { return mParprog; }
            set { mParprog = value; OnPropertyChanged("Parprog"); }
        }
        #endregion

        #region Metodi
        public bool isRicettaPresente()
        {
            if (Nome[0] == '\0')            // se inizio con \0 significa che non c` `e ricetta
                return false;

            return true;
        }
        public void InserisciNome(string NomeImp)
        {
            if (NomeImp.Length < SZ_NOME)
            {
                Populate(Nome, '\0');                       // metto tutti terminatori di stringa
                NomeImp.ToCharArray().CopyTo(Nome, 0);      // copio il nome
            }
        }
        public void InserisciNomeFig(string NomeImp)
        {
            if (NomeImp.Length < SZ_NOME)
            {
                Populate(NomeFig, '\0');                       // metto tutti terminatori di stringa
                NomeImp.ToCharArray().CopyTo(NomeFig, 0);      // copio il nome
            }
        }

        public char[] LeggiNomeFig(bool tipo, string dirFoto)
        {
            string[] nomifile;
            string appoggio = new string(NomeFig);
            appoggio =appoggio.Trim('\0');

            if (Directory.Exists(dirFoto) && appoggio != "")
            {

                nomifile = Directory.GetFiles(dirFoto);
            
                for (int j = 0; j < nomifile.Length; j++)
                {
                    if (nomifile[j].Contains(appoggio) == true)
                          return (appoggio + ".png").ToCharArray(); 
                }
            }
            return (Global.FILE_NOME_VUOTO + ".png").ToCharArray(); 
        }

        public void CopiaProgTo(ClProg Prog)                                   // non usato
        {
   
             //   Prog.Nome = this.Nome;
             //   Prog.NomeFig = this.NomeFig;
           
            for (int i = 0; i < Prog.fase.Length; i++)
                this.fase[i].CopiaTo(Prog.fase[i]);

            Prog.Parprog.num_fase = this.Parprog.num_fase;
            Prog.Parprog.rit_off_ventola = this.Parprog.rit_off_ventola;
            Prog.Parprog.rit_start_ventola = this.Parprog.rit_start_ventola;
            Prog.Parprog.rit_start_bruciatore = this.Parprog.rit_start_bruciatore;

        }
        
      
        
        
        #endregion
    }

    public class ClProgCopy : FunzioniGenerali
    {

        static readonly int NUM_FASI = (8 + 1);
        static readonly int SZ_NOME = (12 + 1);
        static readonly int SZ_PARPROG = (7 + 1);
        public static readonly int SZ_PRG = ((NUM_FASI - 1) * 20 + SZ_NOME * 2 + SZ_PARPROG + 1);

        #region Campi
        private Char[] mNome = new Char[SZ_NOME];
        private Char[] mNomeFig = new Char[SZ_NOME];


        private ClfaseNew[] mfase = new ClfaseNew[NUM_FASI];
        private ClParprog mParprog = new ClParprog();
        #endregion

        #region Costruttore
        public ClProgCopy()
        {

            for (int i = 0; i < fase.Length; i++)
            {
                fase[i] = new ClfaseNew();
                if (i > 0)
                    fase[i].tmp_vapore = 0;

            }
        }
        #endregion

        #region Proprieta
        public Char[] Nome
        {
            get { return mNome; }
            set { mNome = value; OnPropertyChanged("Nome"); }
        }
        public Char[] NomeFig
        {

            get { return mNomeFig; }     // copio il nome  ; }
            set { mNomeFig = value; OnPropertyChanged("NomeFig"); }
        }
        public ClfaseNew[] fase
        {
            get { return mfase; }
            set { mfase = value; OnPropertyChanged("fase"); }
        }
        public ClParprog Parprog
        {
            get { return mParprog; }
            set { mParprog = value; OnPropertyChanged("Parprog"); }
        }
        #endregion

        #region Metodi
        public bool isRicettaPresente()
        {
            if (Nome[0] == '\0')            // se inizio con \0 significa che non c` `e ricetta
                return false;

            return true;
        }
        public void InserisciNome(string NomeImp)
        {
            if (NomeImp.Length < SZ_NOME)
            {
                Populate(Nome, '\0');                       // metto tutti terminatori di stringa
                NomeImp.ToCharArray().CopyTo(Nome, 0);      // copio il nome
            }
        }
        public void InserisciNomeFig(string NomeImp)
        {
            if (NomeImp.Length < SZ_NOME)
            {
                Populate(NomeFig, '\0');                       // metto tutti terminatori di stringa
                NomeImp.ToCharArray().CopyTo(NomeFig, 0);      // copio il nome
            }
        }

        public char[] LeggiNomeFig(bool tipo)
        {
            string appoggio = new string(NomeFig);
            appoggio = appoggio.Trim('\0');
            //  return (appoggio + (tipo ? "_p.png":".png")).ToCharArray(); 
            return (appoggio + ".png").ToCharArray();
        }

        public void CopiaProgTo(ClProgCopy Prog)                                   // non usato
        {

               Prog.Nome = this.Nome;
               Prog.NomeFig = this.NomeFig;

            for (int i = 0; i < Prog.fase.Length; i++)
                this.fase[i].CopiaTo(Prog.fase[i]);

            Prog.Parprog.num_fase = this.Parprog.num_fase;
            Prog.Parprog.rit_off_ventola = this.Parprog.rit_off_ventola;
            Prog.Parprog.rit_start_ventola = this.Parprog.rit_start_ventola;
            Prog.Parprog.rit_start_bruciatore = this.Parprog.rit_start_bruciatore;

        }

        public void WriteFile(string filedacopiare)
        {
         

            if ((Directory.Exists("Disco Rigido") == false))
                  return;



            this.SerializeObject("Disco Rigido\\RICOPY\\" + (filedacopiare));
          }




        public ClProgCopy ReadFile(string filedacopiare)
        {
            if ((Directory.Exists("Disco Rigido") != false))
            {
                return (ClProgCopy)this.DeserializeObject(filedacopiare);
            }
            else
                return null;
           
        }



        #endregion
    } 
    
    
    
    public class ClfaseNew : ClParStr
    {
        #region Campi
        Int16 mset_temp;		// set temperatura regolazione
        Int16 mtmp_cottura;		//  tempo di cottura
        Int16 mtmp_vapore;		//  tempo di vapore	
        Int16 mN_sbuffi;		// numero sbuffi
        Int16 mtmp_sbuffi;		// tempo tra sbuffi
        Int16 mRit_vapore;		//  ritardo tempo di vapore	
        Int16 mtmp_valvola;		// tempo di apertura serranda/scarico tramite valvola 
        Int16 mvelocita_bassa;
        Int16 mpar1;
        Int16 mpar2;


        #endregion

        #region Costruttore
        public ClfaseNew()
        {
            mset_temp = 392;		// set temperatura regolazione
            tmp_cottura = 10;		//  tempo di cottura
            tmp_vapore = 5;		//  tempo di vapore	
            N_sbuffi = 0;		// numero sbuffi
            tmp_sbuffi = 0;		// tempo tra sbuffi
            Rit_vapore = 0;		//  ritardo tempo di vapore	
            tmp_valvola = 0;		// tempo di apertura serranda/scarico tramite valvola 
            velocita_bassa = 0;
            par1=0;
            par2=0;
        }
        #endregion

        #region Proprieta
        public Int16 set_temp
        {
            get { return ConvFtoC(mset_temp, 32); }
            set { mset_temp = MinMax<Int16>(ConvCtoF(value, 32), 32, (short)((SP_tass * 9) / 5 + 32)); OnPropertyChanged("set_temp"); }      // mettere max da par sotterraneo
        }
        public Int16 tmp_cottura
        {
            get { return mtmp_cottura; }
            set { mtmp_cottura = MinMax<Int16>(value, 0, 5999); OnPropertyChanged("tmp_cottura"); }
        }

        public Int16 tmp_vapore
        {
            get { return mtmp_vapore; }
            set { mtmp_vapore = MinMax<Int16>(value, 0, tmax_vap); OnPropertyChanged("tmp_vapore"); }
        }

        public Int16 N_sbuffi
        {
            get { return mN_sbuffi; }
            set { mN_sbuffi = MinMax<Int16>(value, 0, (Int16)(this.tmp_vapore > 0 ? 9 : 0)); OnPropertyChanged("N_sbuffi"); }
        }

        public Int16 tmp_sbuffi
        {
            get { return mtmp_sbuffi; }
            set { mtmp_sbuffi = MinMax<Int16>(value, 0, (Int16)(this.N_sbuffi > 0 ? 999 : 0)); OnPropertyChanged("tmp_sbuffi"); }
        }

        public Int16 Rit_vapore
        {
            get { return mRit_vapore; }
            set { mRit_vapore = MinMax<Int16>(value, 0, this.tmp_cottura); OnPropertyChanged("Rit_vapore"); }
        }

        public Int16 tmp_valvola
        {
            get { return mtmp_valvola; }
            set { mtmp_valvola = MinMax<Int16>(value, 0, this.tmp_cottura); OnPropertyChanged("tmp_valvola"); }
        }

        public Int16 velocita_bassa
        {
            get { return mvelocita_bassa; }
            set { mvelocita_bassa = MinMax<Int16>(value, 0, 1); OnPropertyChanged("velocita_bassa"); }
        }

        public Int16 par1
        {
            get { return mpar1; }
            set { mpar1 = MinMax<Int16>(value, 0, 1); OnPropertyChanged("par1"); }
        }

        public Int16 par2
        {
            get { return mpar2; }
            set { mpar2 = MinMax<Int16>(value, 0, 1); OnPropertyChanged("par2"); }
        }


        #endregion

        #region Metodi
        public Int16 TempCottOre()
        {
            return (Int16)(tmp_cottura / 60);
        }
        public Int16 TempCottMinuti()
        {
            return (Int16)(tmp_cottura % 60);
        }

        #endregion
    }
    public class ClParprog : FunzioniGenerali
    {
        #region Campi
        Byte mnum_fase;		// numero fasi
        Int16 mrit_off_ventola;	// Ritardo spegn.ventole da start vapore
        Int16 mrit_start_ventola;  // Ritardo accens.ventole da fine vapore
        Int16 mrit_start_bruciatore; //Ritardo accens.bruciatore da fine vapore

        #endregion

        #region Costruttore
        public ClParprog()
        {
            num_fase = 1;		// numero fasi
            rit_off_ventola = 0;	// Ritardo spegn.ventole da start vapore
            rit_start_ventola = 2;  // Ritardo accens.ventole da fine vapore
            rit_start_bruciatore = 3; //Ritardo accens.bruciatore da fine vapore
        }
        #endregion

        #region Proprieta

        public Byte num_fase
        {
            get { return mnum_fase; }
            set { mnum_fase = MinMax<Byte>(value, 1, 8); OnPropertyChanged("num_fase"); }
        }

        public Int16 rit_off_ventola
        {
            get { return mrit_off_ventola; }
            set { mrit_off_ventola = MinMax<Int16>(value, 0, 240); OnPropertyChanged("rit_off_ventola"); }
        }

        public Int16 rit_start_ventola
        {
            get { return mrit_start_ventola; }
            set { mrit_start_ventola = MinMax<Int16>(value, 0, 240); OnPropertyChanged("rit_start_ventola"); }
        }

        public Int16 rit_start_bruciatore
        {
            get { return mrit_start_bruciatore; }
            set { mrit_start_bruciatore = MinMax<Int16>(value, 0, 240); OnPropertyChanged("rit_start_bruciatore"); }
        }
        #endregion

        #region Metodi

        #endregion
    }
    public class ClRicette : FunzioniGenerali
    {
        #region NomeFile
        //private readonly string FILE_PROGC = "Progc.xml";
        public readonly string FILE_PROG = "Prog.dat";
    //    private readonly string FILE_USB = "Disco Rigido\\LOG01.TAG";
        #endregion

        #region Costruttore
        public ClRicette()
        {
            Prog = new ClProg[100];
           
            for (int i = 0; i < Prog.Length; i++)
                Prog[i] = new ClProg();

            //Prog[0].InserisciNome(Resource.strmanuale);
            Prog[0].InserisciNomeFig(Global.FILE_NOME_MANUALE);
     
        }
        #endregion

        #region Campi
        public ClProg[] Prog { get; set; }
        #endregion

        #region Metodi
        public void WriteFile()
        {
            //new Thread(() => { this.SerializeObject(CreatePathFile(FILE_PROGC)); }).Start();            // lento con xml

            BinaryWriter bwrtFile = new BinaryWriter(
                    new FileStream(CreatePathFile(FILE_PROG),
                    FileMode.OpenOrCreate,
                    FileAccess.Write,
                    FileShare.None),
                    Encoding.Default);

            Byte[] DatiRicette = ConvRicettaToByte();

            try
            {
                bwrtFile.Write(DatiRicette, 0, DatiRicette.Length);

                bwrtFile.Close();
            }
            catch
            {
            };
        }
        public void ReadFile(string pathFile)
        {
           
            if (File.Exists(pathFile))//CreatePathFile(FILE_PROG)))
            {
                BinaryReader brdFile = new BinaryReader(
                new FileStream(pathFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite), Encoding.Default);
                if (((int)brdFile.BaseStream.Length)<20000){
                    brdFile.Close();
                    File.Delete(pathFile);
                                       
                }


                try
                {
                    Byte[] DatiRicette = brdFile.ReadBytes((int)brdFile.BaseStream.Length);

                    ConvByteToRicetta(DatiRicette);
                    brdFile.Close();
                }
                catch
                {
                }
            }
            //return (ClRicetteC)this.DeserializeObject(CreatePathFile(FILE_PROGC));
        }
        public Byte[] ConvRicettaToByte()
        {
            List<Byte> ListRicetta = new List<byte>();

            //**    METTO I PARAMETRI NEL FORMATO DEL VIDEO KINETIS **

            try
            {

                for (int i = 0; i < Prog.Length; i++)
                {
                    ListRicetta.AddRange(Encoding.ASCII.GetBytes(this.Prog[i].Nome));
                    ListRicetta.AddRange(Encoding.ASCII.GetBytes(this.Prog[i].NomeFig));
                    ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].Parprog.num_fase));
                    ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].Parprog.rit_off_ventola));
                    ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].Parprog.rit_start_ventola));
                    ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].Parprog.rit_start_bruciatore));
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].set_temp));  // converto da short a 2 bytes, controllare se inverte i bytes

                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].tmp_cottura));  // converto da short a 2 bytes, controllare se inverte i bytes

                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].tmp_vapore));  // converto da short a 2 bytes, controllare se inverte i bytes


                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].N_sbuffi));  // converto da short a 2 bytes, controllare se inverte i bytes    


                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].tmp_sbuffi));  // converto da short a 2 bytes, controllare se inverte i bytes    


                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].Rit_vapore));  // converto da short a 2 bytes, controllare se inverte i bytes 


                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].tmp_valvola));  // converto da short a 2 bytes, controllare se inverte i bytes    

                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].velocita_bassa));  // converto da short a 2 bytes, controllare se inverte i bytes    
                
                  for (int j = 0; j < this.Prog[i].fase.Length; j++)
                        ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].par1));  // converto da short a 2 bytes, controllare se inverte i bytes    



                  for (int j = 0; j < this.Prog[i].fase.Length; j++)
                      ListRicetta.AddRange(BitConverter.GetBytes(this.Prog[i].fase[j].par2));  // converto da short a 2 bytes, controllare se inverte i bytes    

                }
            }
            catch
            {

            }

            return ListRicetta.ToArray();
        }
        /*   public Byte[] ConvRicettaToByteSeriale()
           {
               List<Byte> ListRicetta = new List<byte>();

               //**    METTO I PARAMETRI NEL FORMATO DEL VIDEO KINETIS **

               try
               {

                   for (int i = 1; i < Prog.Length; i++)
                   {
                       ListRicetta.AddRange(Encoding.ASCII.GetBytes(this.Prog[i].Nome));
                       ListRicetta.AddRange(Encoding.ASCII.GetBytes(this.Prog[i].NomeFig));

                       int IndBooster = this.Prog[i].Fase.Length - 10;

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setcott);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.AddRange(BitConverter.GetBytes(this.Progc[i].Fase[j].mSetcamera));  // converto da short a 2 bytes, controllare se inverte i bytes
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster + 1].Setbooster));
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster].Setbooster));
                       IndBooster += 2;

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setcielo);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setpotenza);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setvapl);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setvaps);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[j].Setcamino));
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           ListRicetta.Add(this.Progc[i].Fase[j].Setvent);
                       ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[IndBooster++].Setbooster));

                       for (int j = 0; j < this.Progc[i].Fase.Length - 10; j++)
                           ListRicetta.Add(Convert.ToByte(this.Progc[i].Fase[j].Setbooster));
                   }
               }
               catch
               {

               }

               return ListRicetta.ToArray();
           }*/
        public int ConvByteToRicetta(Byte[] Buf)
        {
            //**    CARICO I PARAMETRI DALL` LCD **

            int Index = 0;

            try
            {

                for (int i = 0; i < Prog.Length; i++)
                {
                    Index += Encoding.ASCII.GetChars(Buf, Index, Prog[i].Nome.Length, this.Prog[i].Nome, 0);
                    Index += Encoding.ASCII.GetChars(Buf, Index, Prog[i].Nome.Length, this.Prog[i].NomeFig, 0);
                    this.Prog[i].Parprog.num_fase = Buf[Index];
                    Index += 2;
                    this.Prog[i].Parprog.rit_off_ventola= BitConverter.ToInt16(Buf, Index);
                    Index += 2;
                    this.Prog[i].Parprog.rit_start_ventola = BitConverter.ToInt16(Buf, Index);
                    Index += 2;
                    this.Prog[i].Parprog.rit_start_bruciatore = BitConverter.ToInt16(Buf, Index);
                    Index += 2;

                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].set_temp = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].tmp_cottura = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].tmp_vapore = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].N_sbuffi = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].tmp_sbuffi = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].Rit_vapore = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].tmp_valvola = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }

                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].velocita_bassa = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].par1 = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }
                    for (int j = 0; j < this.Prog[i].fase.Length; j++)
                    {
                        this.Prog[i].fase[j].par2 = BitConverter.ToInt16(Buf, Index);
                        Index += 2;
                    }

                }

                return 0;
            }
            catch
            {
                return -1;                                  // error               
            }
        }
        /*   public int ConvByteToRicettaUSB(Byte[] Buf)
           {
               //**    CARICO I PARAMETRI DALLA CHIAVETTA**
               int Index = 8;

               int NumProg = Buf[3];                   // carico il numero programma

               try
               {
                   for (int i = 1; i <= NumProg; i++)
                   {
                       Index += Encoding.ASCII.GetChars(Buf, Index, Progc[i].Nome.Length, this.Progc[i].Nome, 0);
                       this.Progc[i].Prot = Buf[Index++];

                       int IndBooster = this.Progc[i].Fase.Length - 10;

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setcott = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                       {
                           this.Progc[i].Fase[j].mSetcamera = BitConverter.ToInt16(Buf, Index);
                           Index += 2;
                       }
                       this.Progc[i].Fase[IndBooster + 1].Setbooster = Convert.ToBoolean(Buf[Index++]);
                       this.Progc[i].Fase[IndBooster].Setbooster = Convert.ToBoolean(Buf[Index++]);
                       IndBooster += 2;

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setcielo = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setpotenza = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setvapl = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setvaps = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setcamino = Convert.ToBoolean(Buf[Index++]);
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 1; j++)
                           this.Progc[i].Fase[j].Setvent = Buf[Index++];
                       this.Progc[i].Fase[IndBooster++].Setbooster = Convert.ToBoolean(Buf[Index++]);

                       for (int j = 0; j < this.Progc[i].Fase.Length - 10; j++)
                           this.Progc[i].Fase[j].Setbooster = Convert.ToBoolean(Buf[Index++]);
                   }

                   return 0;
               }
               catch
               {
                   return -1;                                  // error               
               }
           }*/
        /*   public int WriteRicettaUSB()
           {
               if (NprogMem() < 0)
                   return -1;

               List<Byte> ListRicetta = new List<byte>();
               ListRicetta.Add(1);                 //versione
               ListRicetta.Add(0);
               ListRicetta.Add(0);
               ListRicetta.Add((byte)NprogMem());        // numero programma
               ListRicetta.Add(0);
               ListRicetta.Add(0);
               ListRicetta.Add(0);
               ListRicetta.Add(0);
               ListRicetta.AddRange(ConvRicettaToByteSeriale().Take(NprogMem() * ClProgc.SZ_PRG));

               try
               {
                   BinaryWriter bwrtFile = new BinaryWriter(
                           new FileStream(FILE_USB,
                           FileMode.OpenOrCreate,
                           FileAccess.Write,
                           FileShare.None),
                           Encoding.Default);

                   Byte[] DatiRicette = ListRicetta.ToArray();
                   bwrtFile.Write(DatiRicette, 0, DatiRicette.Length);
                   bwrtFile.Close();
                   return 0;
               }
               catch
               {
                   return -1;
               };
           }*/
        /*   public void ReadRicettaUSB()
           {
               if (File.Exists(FILE_USB))
               {
                   BinaryReader brdFile = new BinaryReader(
                       new FileStream(FILE_USB, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite), Encoding.Default);

                   try
                   {
                       Byte[] DatiRicette = brdFile.ReadBytes((int)brdFile.BaseStream.Length);
                       ConvByteToRicettaUSB(DatiRicette);
                       WriteFile();
                       brdFile.Close();
                   }
                   catch
                   {
                   }
               }
           }  */
        private int NprogMem()
        {
            int Len = Prog.Length - Prog.Reverse().TakeWhile(i => i.isRicettaPresente() == false).Count() - 1;    // Indice ultimo programmma creato
            return Len;
        }
        #endregion
    }

    #endregion



    #region VariabiliGlobali
    public enum DimImageRicetta
    {
        WIDTH = 240,
        HEIGHT = 175,
        WIDTH_P = 150,
        HEIGHT_P = 109,

    };
    public static class Global
    {

        static internal string FILE_NOME_MANUALE = "mano";
        static internal string FILE_NOME_VUOTO = "vuoto";

        public static ClParStr ParStr;

        public static FtpClient conn;

        public static ClRicette ricette;
    }

        #endregion

    }