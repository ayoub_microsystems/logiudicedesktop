﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class frmProgram : Form
    {
        frmGestioneRicetta formGestRic = null;
        int pop = 0;
        //ClProg prog = new ClProg();

        //public static ClProg Prog { get => prog; set => prog = value; }

        public frmProgram()
        {
            InitializeComponent();

            //Global.ricette.Prog[0].

            CaricaRicette();


        }
        private string LeggiNomeFig(char[] nome)
        {
            string appoggio = new string(nome);
            appoggio = appoggio.Trim('\0');
            //  return (appoggio + (tipo ? "_p.png":".png")).ToCharArray(); 
            return (appoggio + ".png");
        }

        private void FrmProgram_Click(object sender, EventArgs e)
        {
            if (formGestRic == null)
                formGestRic = new frmGestioneRicetta();

            int indice = ((UserControl1)sender).TabIndex;
            if(((DialogResult)formGestRic.ShowDialog(ref Global.ricette.Prog[indice])) == DialogResult.Yes)
            {
                //Salvo il file delle ricette
                Global.ricette.WriteFile();
                CaricaRicette();
            }
        }


        private void CaricaRicette()
        {
            flowLayoutPanel1.Controls.Clear();

            List<UserControl1> _itemRicette = new List<UserControl1>();

            for (int i = 0; i < Global.ricette.Prog.Length; i++)
            {
                if (Global.ricette.Prog[i].isRicettaPresente())
                {
                    UserControl1 ucr = new UserControl1();
                    ucr.NomeRic = new String(Global.ricette.Prog[i].Nome);
                    ucr.ImgRic = Image.FromFile(@"C:\DatiFornoRotor\Immagini\" + new String(Global.ricette.Prog[i].LeggiNomeFig(true, @"C:\DatiFornoRotor\Immagini\")));
                    _itemRicette.Add(ucr);
                }
            }

            for (int i = 0; i < _itemRicette.Count; i++)
            {
                _itemRicette[i].Click += FrmProgram_Click;
                flowLayoutPanel1.Controls.Add(_itemRicette[i]);
            }
        }

        private void timerOrologio_Tick(object sender, EventArgs e)
        {

        }

        private void btnNuovaRic_Click(object sender, EventArgs e)
        {
            int i = 0;
            frmNuovaRicetta nr = new frmNuovaRicetta();

            //Gli passo la prima ricetta libera
            for (i = 0; i < Global.ricette.Prog.Length; i++)
            {
                if (!Global.ricette.Prog[i].isRicettaPresente())
                    break;
            }

            if ((DialogResult)nr.ShowDialog(ref Global.ricette.Prog[i]) == DialogResult.OK)
            {
                if (formGestRic == null)
                    formGestRic = new frmGestioneRicetta();

                if (((DialogResult)formGestRic.ShowDialog(ref Global.ricette.Prog[i])) == DialogResult.Yes)
                {
                    //Salvo il file delle ricette
                    Global.ricette.WriteFile();
                    CaricaRicette();

                }
            }
        }

        private void btnChiudi_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
