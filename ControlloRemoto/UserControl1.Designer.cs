﻿namespace ControlloRemoto
{
    partial class UserControl1
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomeRic = new System.Windows.Forms.Label();
            this.pbImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomeRic
            // 
            this.lblNomeRic.AutoSize = true;
            this.lblNomeRic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeRic.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblNomeRic.Location = new System.Drawing.Point(0, 127);
            this.lblNomeRic.Name = "lblNomeRic";
            this.lblNomeRic.Size = new System.Drawing.Size(153, 20);
            this.lblNomeRic.TabIndex = 1;
            this.lblNomeRic.Text = "AAAAAAAAAAAA";
            this.lblNomeRic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNomeRic.Click += new System.EventHandler(this.lblNomeRic_Click);
            // 
            // pbImage
            // 
            this.pbImage.BackColor = System.Drawing.Color.LightYellow;
            this.pbImage.Location = new System.Drawing.Point(1, 11);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(150, 108);
            this.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            this.pbImage.Click += new System.EventHandler(this.pbImage_Click);
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.pbImage);
            this.Controls.Add(this.lblNomeRic);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(152, 156);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNomeRic;
        private System.Windows.Forms.PictureBox pbImage;
    }
}
