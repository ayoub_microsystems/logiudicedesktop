﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{    
    public partial class frmGestioneFasi : Form
    {
        DialogResult azione = DialogResult.Cancel;

        Point[] coordPannelloFasi = 
        {
            new Point ( 189,3 ), //fasi totali 1
            new Point ( 286,3 ), //fasi totali 2
            new Point ( 383,3 ), //fasi totali 3
            new Point ( 480,3 ), //fasi totali 4
            new Point ( 576,3 ), //fasi totali 5
            new Point ( 674,3 ), //fasi totali 6
            new Point ( 771,3 ), //fasi totali 7
            new Point ( 872,3 )  //fasi totali 8

    };

       Point[] coordPannelloVelocita = 
        {
            new Point ( 1,512 ), //NASCOSTO
            new Point( 872, 512 )  //VISIBILE
        };

        List<TextBox> textBoxTemperatura = new List<TextBox>();
        List<MaskedTextBox> textBoxTempo = new List<MaskedTextBox>();
        List<TextBox> textBoxTempoVap = new List<TextBox>();
        List<TextBox> textBoxRipetizioniVap = new List<TextBox>();
        List<TextBox> textBoxPausaVap = new List<TextBox>();
        List<TextBox> textBoxRitardoVap = new List<TextBox>();
        List<TextBox> textBoxTempoValvola = new List<TextBox>();
        List<TextBox> textBoxVelocita = new List<TextBox>();

        public frmGestioneFasi()
        {
            InitializeComponent();

            textBoxTemperatura.Add(txbTemperaturaFase1);
            textBoxTemperatura.Add(txbTemperaturaFase2);
            textBoxTemperatura.Add(txbTemperaturaFase3);
            textBoxTemperatura.Add(txbTemperaturaFase4);
            textBoxTemperatura.Add(txbTemperaturaFase5);
            textBoxTemperatura.Add(txbTemperaturaFase6);
            textBoxTemperatura.Add(txbTemperaturaFase7);
            textBoxTemperatura.Add(txbTemperaturaFase8);

            textBoxTempo.Add(txbTempoCottura1);
            textBoxTempo.Add(txbTempoCottura2);
            textBoxTempo.Add(txbTempoCottura3);
            textBoxTempo.Add(txbTempoCottura4);
            textBoxTempo.Add(txbTempoCottura5);
            textBoxTempo.Add(txbTempoCottura6);
            textBoxTempo.Add(txbTempoCottura7);
            textBoxTempo.Add(txbTempoCottura8);

            textBoxTempoVap.Add(txbTempoVap1);
            textBoxTempoVap.Add(txbTempoVap2);
            textBoxTempoVap.Add(txbTempoVap3);
            textBoxTempoVap.Add(txbTempoVap4);
            textBoxTempoVap.Add(txbTempoVap5);
            textBoxTempoVap.Add(txbTempoVap6);
            textBoxTempoVap.Add(txbTempoVap7);
            textBoxTempoVap.Add(txbTempoVap8);

            textBoxRipetizioniVap.Add(txbNSbuffi1);
            textBoxRipetizioniVap.Add(txbNSbuffi2);
            textBoxRipetizioniVap.Add(txbNSbuffi3);
            textBoxRipetizioniVap.Add(txbNSbuffi4);
            textBoxRipetizioniVap.Add(txbNSbuffi5);
            textBoxRipetizioniVap.Add(txbNSbuffi6);
            textBoxRipetizioniVap.Add(txbNSbuffi7);
            textBoxRipetizioniVap.Add(txbNSbuffi8);

            textBoxPausaVap.Add(txbPausaSbuffi1);
            textBoxPausaVap.Add(txbPausaSbuffi2);
            textBoxPausaVap.Add(txbPausaSbuffi3);
            textBoxPausaVap.Add(txbPausaSbuffi4);
            textBoxPausaVap.Add(txbPausaSbuffi5);
            textBoxPausaVap.Add(txbPausaSbuffi6);
            textBoxPausaVap.Add(txbPausaSbuffi7);
            textBoxPausaVap.Add(txbPausaSbuffi8);

            textBoxRitardoVap.Add(txbRitardoVap1);
            textBoxRitardoVap.Add(txbRitardoVap2);
            textBoxRitardoVap.Add(txbRitardoVap3);
            textBoxRitardoVap.Add(txbRitardoVap4);
            textBoxRitardoVap.Add(txbRitardoVap5);
            textBoxRitardoVap.Add(txbRitardoVap6);
            textBoxRitardoVap.Add(txbRitardoVap7);
            textBoxRitardoVap.Add(txbRitardoVap8);

            textBoxTempoValvola.Add(txbTempoValvola1);
            textBoxTempoValvola.Add(txbTempoValvola2);
            textBoxTempoValvola.Add(txbTempoValvola3);
            textBoxTempoValvola.Add(txbTempoValvola4);
            textBoxTempoValvola.Add(txbTempoValvola5);
            textBoxTempoValvola.Add(txbTempoValvola6);
            textBoxTempoValvola.Add(txbTempoValvola7);
            textBoxTempoValvola.Add(txbTempoValvola8);

            textBoxVelocita.Add(txbVelocita1);
            textBoxVelocita.Add(txbVelocita2);
            textBoxVelocita.Add(txbVelocita3);
            textBoxVelocita.Add(txbVelocita4);
            textBoxVelocita.Add(txbVelocita5);
            textBoxVelocita.Add(txbVelocita6);
            textBoxVelocita.Add(txbVelocita7);
            textBoxVelocita.Add(txbVelocita8);

        }

        public object ShowDialog(ref ClProg prog)
        {
            this.Text = new String(prog.Nome) + " : gestione fasi";

            pnlMascheraFasi.Location = coordPannelloFasi[Math.Max(0, prog.Parprog.num_fase - 1)];
            pnlMascheraVelocita.Location = coordPannelloVelocita[0];

            for(int i = 0; i < prog.Parprog.num_fase; i++)
            {
                textBoxTemperatura[i].Text = prog.fase[i].set_temp.ToString();
                textBoxTempo[i].Text = prog.fase[i].TempCottOre().ToString("D2") + ":" + prog.fase[i].TempCottMinuti().ToString("D2");
                textBoxTempoVap[i].Text = prog.fase[i].tmp_vapore.ToString();
                textBoxRipetizioniVap[i].Text = prog.fase[i].N_sbuffi.ToString();
                textBoxPausaVap[i].Text = prog.fase[i].tmp_sbuffi.ToString();
                textBoxRitardoVap[i].Text = prog.fase[i].Rit_vapore.ToString();
                textBoxTempoValvola[i].Text = prog.fase[i].tmp_valvola.ToString();
                textBoxVelocita[i].Text = prog.fase[i].velocita_bassa.ToString();
            }

            DialogResult dre = base.ShowDialog();

            if(azione == DialogResult.OK)
            {
                for (int i = 0; i < prog.Parprog.num_fase; i++)
                {
                    prog.fase[i].set_temp = Convert.ToInt16(textBoxTemperatura[i].Text);
                    //prog.fase[i].tmp_cottura = textBoxTempo[i].Text;
                    prog.fase[i].tmp_vapore = Convert.ToInt16(textBoxTempoVap[i].Text);
                    prog.fase[i].N_sbuffi = Convert.ToInt16(textBoxRipetizioniVap[i].Text);
                    prog.fase[i].tmp_sbuffi = Convert.ToInt16(textBoxPausaVap[i].Text);
                    prog.fase[i].Rit_vapore = Convert.ToInt16(textBoxRitardoVap[i].Text);
                    prog.fase[i].tmp_valvola = Convert.ToInt16(textBoxTempoValvola[i].Text);
                    prog.fase[i].velocita_bassa = Convert.ToInt16(textBoxVelocita[i].Text);
                }

                return DialogResult.OK;
            }

            return DialogResult.Cancel;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
            {
                textBoxTemperatura[i].Text = "200";
                textBoxTempo[i].Text = "00:10";
                if(i == 0)
                    textBoxTempoVap[i].Text = "5";
                else
                    textBoxTempoVap[i].Text = "0";
                textBoxRipetizioniVap[i].Text = "0";
                textBoxPausaVap[i].Text = "0";
                textBoxRitardoVap[i].Text = "0";
                textBoxTempoValvola[i].Text = "0";
                textBoxVelocita[i].Text = "0";
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            azione = DialogResult.OK;

            this.Close();
            
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("MASKET INPUT REJECTED");
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("TEXT CHANGED");
        }

        private void txbTempoVap1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(((TextBox)sender).Text) == 0)
                {
                    //((TextBox)sender).Text = "0";
                    txbNSbuffi1.Text = "0";
                    txbNSbuffi1.Enabled = false;
                }
                else
                {
                    txbNSbuffi1.Enabled = true;
                    if (Convert.ToInt16(((TextBox)sender).Text) > 10)
                    {
                        ((TextBox)sender).Text = "10";
                    }
                }
            }
            catch(FormatException fe)
            {
                System.Diagnostics.Debug.WriteLine(fe.Message);
                ((TextBox)sender).Text = "0";
            }
        }


        private void txbNSbuffi1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(((TextBox)sender).Text) == 0)
                {
                    //((TextBox)sender).Text = "0";
                    txbPausaSbuffi1.Text = "0";
                    txbPausaSbuffi1.Enabled = false;
                }
                else
                {
                    txbPausaSbuffi1.Enabled = true;
                    if (Convert.ToInt16(((TextBox)sender).Text) > 9)
                        ((TextBox)sender).Text = "9";
                }
            }
            catch (FormatException fe)
            {
                System.Diagnostics.Debug.WriteLine(fe.Message);
                ((TextBox)sender).Text = "0";
            }
        }

        private void txbPausaSbuffi1_TextChanged(object sender, EventArgs e)
        {
            try
            {
               if (Convert.ToInt16(((TextBox)sender).Text) > 999)
                    ((TextBox)sender).Text = "999";
               
            }
            catch (FormatException fe)
            {
                System.Diagnostics.Debug.WriteLine(fe.Message);
                ((TextBox)sender).Text = "0";
            }
        }
    
                    
    }
}
