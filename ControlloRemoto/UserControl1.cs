﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlloRemoto
{
    public partial class UserControl1 : UserControl
    {
        private string _nomeRic;
        private Image _img;
       


        public UserControl1()
        {
            InitializeComponent();
        }

        public string NomeRic
        {
            get { return _nomeRic; }
            set
            {
                _nomeRic = value;
                lblNomeRic.Text = _nomeRic;
            }
        }
        public Image ImgRic
        {
            get { return _img; }
            set
            {
                _img = value;
                pbImage.Image = _img;
            }
        }

        //private void UserControl1_Click(object sender, EventArgs e)
        //{

        //}


        private void pbImage_Click(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, e);
        }

        
        private void lblNomeRic_Click(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, e);
        }
    }
}
